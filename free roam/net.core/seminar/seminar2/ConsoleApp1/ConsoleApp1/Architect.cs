﻿using System;

namespace ConsoleApp1
{
    public class Architect : Employee
    {
        //public override string JobTitle => "Architect";
        public Architect(string firstName, string lastName, DateTime birthDate)
            : base(firstName, lastName, birthDate)
        {
        }
    };

    public class TechLead : Employee
    {
        public TechLead(string firstName, string lastName, DateTime birthDate)
            : base(firstName, lastName, birthDate)
        {
        }
    }
    
    public class Developer : Employee
    {
        public Developer(string firstName, string lastName, DateTime birthDate)
            : base(firstName, lastName, birthDate)
        {
        }
    }
}
