﻿using System;

namespace ConsoleApp1
{
    public abstract class Employee
    {
        public Employee(string firstName, string lastName, DateTime birthDate)
        {
            Id = Guid.NewGuid();
            FirstName = firstName;
            LastName = lastName;
            BirthDate = birthDate;
        }
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthDate { get; set; }

        public string JobTitle { get; }

        public int ComputeAge()
        {
            return DateTime.Now.Year - BirthDate.Year;
        }
    }
}
