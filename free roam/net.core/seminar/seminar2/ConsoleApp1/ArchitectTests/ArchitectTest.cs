
using ConsoleApp1;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Xunit.Sdk;

namespace ArchitectTests
{
    [TestClass]
    public class ArchitectTest
    {
        [TestMethod]
        public void GivenANewArchitect_WhenBirthDateIs2000_ThenAgeShouldBe19()
        {
            // arrange
            Architect architect = new Architect("John", "Doe", new DateTime(200, 10, 7));

            // act
            int age = architect.ComputeAge();

            // assert
            Assert.IsTrue(age == 19);
        }

        [TestMethod]
        public void Test2()
        {
            // arrange
            Architect architect = new Architect("John", "Doe", new DateTime(200, 10, 7));
            string jobTitle = architect.JobTitle;
            // act
            int age = architect.ComputeAge();

            // assert
            Assert.IsTrue(age == 19);
        }
    }
}
