var express = require('express');
var router = express.Router();

var trainerController = require('../controllers/trainerController.js');

router.get('/', trainerController.getTrainers);

router.get('/:id', trainerController.getTrainerById);

module.exports = router;