var express = require('express');


const app = express();



const trainerRoutes = require('./routes/trainerRoutes');
app.use('/v1/trainer', trainerRoutes);
app.get('', (req, res) => {
    res.send('hello express!')
})

app.listen(3000, () => console.log('Server running on port 3000!')) 