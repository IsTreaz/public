var Trainer = require('../entities/Trainer')

let trainers = [];
 trainers.push(new Trainer("lala"));
 trainers.push(new Trainer("lala"));
 trainers.push(new Trainer("lala"));
// console.log(trainers);

exports.GetAll = () => {
    return trainers;
}
exports.GetById = (id) => {
    return trainers[id];
}
exports.Create = (trainer) => {
    trainers.push(trainer);
}
exports.Update = (id, trainer) => {
    trainers[id] = trainer;
}
exports.Delete = (id) => {
    trainers.splice(id, 1);
}