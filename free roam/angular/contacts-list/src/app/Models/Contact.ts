import * as Immutable from 'immutable';
import { createStore } from 'redux';
import { reducer } from './reducer';
import { IContactAction } from './actions';

export class ContactModel {
  id: number;
  name: string;
  star: boolean;
}

export class ContactStore {
  store = createStore(reducer, Immutable.List<ContactModel>())

  public get contacts(): Immutable.List<ContactModel> {
    return this.store.getState();
  }

  dispatch(action: IContactAction) {
    this.store.dispatch(action);
  }

}

