import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { ContactModel, ContactStore } from '../Models/Contact';
import { removeContact, starContact } from '../Models/actions';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContactComponent {

  @Input() contact: ContactModel;

  constructor(private store: ContactStore) { }

  public removeContact(contact) {
    this.store.dispatch(removeContact(this.contact.id));
  }

  public starContact(contact) {
    this.store.dispatch(starContact(this.contact.id));
  }


}
