import { Component, OnInit } from '@angular/core';
import { ContactStore } from '../Models/Contact';
import { addContact } from '../Models/actions';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent {

  private contactId: number;
  constructor(public store: ContactStore) {
    this.contactId = 0;
  }
  addContact(contact): void {
    this.store.dispatch(addContact(contact, this.contactId++))
  }


}
