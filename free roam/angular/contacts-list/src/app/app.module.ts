import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactStore } from './Models/Contact';
import { HighlightDirective } from './contact-list/directives/highlight.directive';
import { ContactComponent } from './contact/contact.component';

@NgModule({
  declarations: [
    AppComponent,
    ContactListComponent,
    HighlightDirective,
    ContactComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ContactStore],
  bootstrap: [AppComponent]
})
export class AppModule { }
