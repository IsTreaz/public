import { Component, NgZone } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'change-detection-prototype';
  thicks = 0;
  /**
   *
   */
  constructor(private _zone: NgZone) {
    this._zone.onMicrotaskEmpty.subscribe(() => {
      this.thicks++;
    });
  }
}
