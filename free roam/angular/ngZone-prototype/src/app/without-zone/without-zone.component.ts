import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-without-zone',
  templateUrl: './without-zone.component.html',
  styleUrls: ['./without-zone.component.css']
})
export class WithoutZoneComponent implements OnInit {

  constructor() { }

  text = 'Do not click anymore';
  counter = 0;
  ngOnInit() {
  }

  btnClick() {
    console.log('fn 1');
    this.counter++;
    if (this.counter === 5) {
      this.text += '!';
      this.counter = 0;
    }
  }
}
