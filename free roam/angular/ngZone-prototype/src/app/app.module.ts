import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WithoutZoneComponent } from './without-zone/without-zone.component';
import { WithZoneComponent } from './with-zone/with-zone.component';

@NgModule({
  declarations: [
    AppComponent,
    WithoutZoneComponent,
    WithZoneComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
