import { Component, OnInit, NgZone, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/compiler/src/core';

@Component({
  selector: 'app-with-zone',
  templateUrl: './with-zone.component.html',
  styleUrls: ['./with-zone.component.css']
})
export class WithZoneComponent implements OnInit {

  constructor(private _zone: NgZone, private _elRef: ElementRef, private _cd: ChangeDetectorRef) { }

  times = {};
  counter = 0;
  text = 'Do not click anymore';

  ngOnInit() {

    const buttons = this._elRef.nativeElement.childNodes[0].childNodes;

    this._zone.runOutsideAngular(() => {

      buttons.forEach((btn) => {
        console.log(btn);

        btn.addEventListener('click', (elem) => {
          this.counter ++;
          console.log('  -->: this.counter', this.counter)

          if (this.counter === 5) {
            this.text += '!';
            this.counter = 0;
            this.paint();
          }
        });
      });
    });
  }

  paint() {
    this._zone.run( () => this._cd.detectChanges())
  }
}
