﻿using System;

namespace Key2Pokemons.Domain
{
    public class Trainer : Entity
    {
        private Trainer(string name)
        {
            this.Name = Name.Create(name);
            this.Level = 0;
        }

        public static Trainer Create(string name)
        {
            return new Trainer(name);
        }

        public  Name Name { get; private set; }

        public int Level { get; private set; }

        public void LevelUp()
        {
            Level++;
        }

        public void ChangeName(string name)
        {
           this.Name = Name.Create(name);
        }
    }
}