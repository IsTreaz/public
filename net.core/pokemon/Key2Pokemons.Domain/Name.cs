﻿using System.Collections.Generic;
using CSharpFunctionalExtensions;

namespace Key2Pokemons.Domain
{
    public sealed class Name: ValueObject
    {
        public string Value { get; private set; }

        private Name(string name)
        {
            if (name.Length > 5 && name.Length < 20)
            {
                this.Value = name;
            }
        }

        public static Name Create(string name)
        {
            return new Name(name);
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
    }
}