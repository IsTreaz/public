%{
#include <stdio.h>
%}
%x parinte
cuv_lista1 [b-z]+[a-z]*|a+[c-z]+[a-z]*
elem_lista1 a[c-z][a-z]*|a[a]+[a-z]*|a|b|[b-z][a-z]*|[3-9][0-9][0-9]|[1-9][0-9][0-9][0-9]+
elem_lista2 [a-z]+,[ ]*[a-z]+(,[ ]*[a-z]+)*[ ]*[\n]
orice_fara_enter [^\n]
%%
parinte[ ]*\< {BEGIN parinte;}
<parinte>"lista1"[ ]*"{"[ ]*{elem_lista1}(,[ ]*{elem_lista1})*[ ]*"}" {printf ("%s\n", yytext); }
<parinte>"lista2-"[ ]*[a-z]+[ ]+[a-z]+([ ]*[a-z]+)+[ ]*[\n] {printf ("%s\n", yytext); }

<parinte>\> {BEGIN 0;}
<parinte>.|\n ;
.|\n ;

%%
int main(int argc, char** argv){
if(argc>0)
 yyin = fopen(argv[1],"r");
yylex();
}