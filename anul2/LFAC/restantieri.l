%{
    #include <stdio.h>

%}
%x good
cifra [0-9]
litera [a-z]
spatiu [ ]
interior {cifra}*{litera}*{spatiu}*
interior2 {interior}+
%%
"<ul>"|"<ol>" {BEGIN good;}
<good>"<li>"{interior2}"</li>" {ECHO;}
<good>"</ul>"|"</ol>" {BEGIN 0;}
<good>.|\n ;
.|\n ;
%%
int main(int argc, char ** argv){
    if(argc>0)
        yyin = fopen(argv[1], "r");
    yylex();
}