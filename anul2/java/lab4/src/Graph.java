import java.io.*;
import java.util.ArrayList;

public class Graph {
    private String name;
    private String pathTGF;
    private String pathImage;
    private ArrayList<Integer> nodes;
    private ArrayList<Edge> edges;

    public Graph(String name, String pathTGF, String pathImage) throws  NotFound, EmptyFile {
        nodes = new ArrayList<Integer>();
        edges = new ArrayList<Edge>();
        this.name = name;
        this.pathTGF = pathTGF;
        this.pathImage = pathImage;
        int ok =0;
        File file = new File(pathTGF);


        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String line;
            while((line  = bufferedReader.readLine()) != null)
            {
                if(line.contains("#")==true)
                {
                    ok ++;
                }
                else {
                    if(ok == 1)
                    {
                        char n1 = line.charAt(0);
                        int n = n1 - '0';
                        nodes.add(n);

                    }
                    else
                    {
                        char n1 = line.charAt(0);
                        char n2 = line.charAt(2);
                        int n = n2 -'0';
                        edges.add(new Edge(n1 - '0', n));

                    }
                }
            }
        } catch (FileNotFoundException e ) {
            throw new NotFound();
        }
        catch (NullPointerException e){
            throw new EmptyFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ;
    }

    public void report()
    {

    }


    public String getName()
    {
        return this.name;
    }
    public String getPathTGF()
    {
        return this.getPathTGF();
    }
    public String getPathImage()
    {
        return this.pathImage;
    }


    @Override
    public String toString() {
        return "Graph{" +
                "name='" + name + '\'' +
                ", pathTGF='" + pathTGF + '\'' +
                ", pathImage='" + pathImage + '\'' +
                ", nodes=" + nodes +
                ", edges=" + edges +
                '}';
    }
}
