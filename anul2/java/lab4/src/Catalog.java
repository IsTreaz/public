import java.awt.*;
import java.io.*;
import java.awt.desktop.*;
import java.util.ArrayList;
import java.util.List;

public class Catalog  {
    List<Graph> graphs = new ArrayList<>();
    String catalogPath;

    public Catalog(String catalogPath) {
        this.catalogPath = catalogPath;
    }

    public void add(Graph graph)
    {
        this.graphs.add(graph);
    }

    public void open(String name) throws LoadingException{
        for(Graph i: this.graphs){
            if(i.getName().equals(name)){
                Desktop desktop= Desktop.getDesktop();
                File image= new File(i.getPathImage());
                try {
                    desktop.open(image);
                } catch (IOException e) {
                    throw new LoadingException();
                }
            }
        }
    }

    public void save(String cat) throws LoadingException {
        try {
            String dm = this.catalogPath + "/" + cat;
            BufferedWriter writer = null;
            File file = new File(dm);
            writer = new BufferedWriter(new FileWriter(file));
            for (Graph g : graphs) {
                writer.write(g.toString());
            }
            writer.close();
        } catch (IOException e) {
            throw new LoadingException();
        }
    }

    public void load(String cat)
    {

    }

    public void list()
    {
        for(Graph graph : graphs)
        {
            System.out.println(graph.getName());
        }
    }
}
