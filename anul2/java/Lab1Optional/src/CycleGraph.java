public class CycleGraph extends Graph {
    public CycleGraph(int dimension)
    {
        this.dimension = dimension;
        matrix = new int[dimension][dimension];
        for(int i=0;i<dimension;i++)
            for(int j=0;j<dimension;j++)
                matrix[i][j] = 0;

        for(int i=0;i<dimension-1;i++)
            matrix[i][i+1] = matrix[i+1][i] = 1;
        matrix[dimension-1][0] = matrix[0][dimension-1] = 1;

        this.initialize();
    }

}
