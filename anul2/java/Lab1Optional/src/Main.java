import java.sql.Timestamp;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
//        import java.util.Scanner;

//...
        long startTime = System.currentTimeMillis();

        Scanner in = new Scanner(System.in);

        int num = Integer.parseInt(args[0]);
        int tipGraf = Integer.parseInt(args[1]);
        if(num%2 == 0)
        {
            System.out.println("NUMARUL NU ESTE PAR. ");

            while(num % 2 == 0)
            {
                System.out.println("introduce-ti un numar IMPAR");
                num = in.nextInt();
            }
        }
        System.out.println("Numarul introdus este IMPAR. Continuam ...");






        Graph  graph;
        switch (tipGraf){
            case 1: tipGraf = 1;
                graph = new CompleteGraph(num);
                break;
            case 2: tipGraf = 2;
                graph = new CycleGraph(num);
                break;
            case 3: tipGraf = 3;
                graph = new RandomGraph(num);
                break;
            case 4: tipGraf =4;
                graph = new Arbore(num);
                break;
            default:
                graph = new Arbore(num);
                break;
        }
        if(num < 30000)
            graph.print();
        System.out.println("");
        System.out.println("Numarul de muchii al grafului este: ");
        System.out.println(graph.edgeCounter);


        System.out.println("Numarul de muchii maxim al unui nod este: ");
        System.out.println("\u0394(G)=" + graph.maximumDegree);

        System.out.println("");
        System.out.println("Numarul de muchii minim al unui nod este: ");
        System.out.println("\u03b4(G)=" + graph.minimumDegree);

        System.out.println("");
        if(graph.isRegular()==true)
            System.out.println("Graful este regulat");
        else
            System.out.println(("Graful nu este regulat"));
        System.out.println("");
        System.out.println("Numarul de muchii este egal cu dublul sumei gradului nodurilor");


        long endTime = System.currentTimeMillis();

        System.out.println("Timpul total de executie este: " + (endTime - startTime) +" milisecunde");
        System.out.println("");
        int n = graph.nrConnectedComponents();
        if(n == num)
            System.out.println("Graful este conex");
        else
            System.out.println("Graful are: " + n +" componente conexe");
    }
}
