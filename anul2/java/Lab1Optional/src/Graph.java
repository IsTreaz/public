public abstract class Graph {
    public int[][] matrix;
    public int dimension;
    public int minimumDegree;
    public int maximumDegree;
    public int edgeCounter;
    public int degreeSum;

    //    public void setDimension(int dimension)
//    {
//        this.dimension = dimension;
//    }
    protected void initialize()
    {
        this.minMaxDegreez();
        this.edgeNumber();
    }

    private void printLines()
    {
        System.out.println("");
        System.out.print("\t\t ");
        for(int j=0;j<dimension*2;j++)
        {
            if(j%2==1)
                System.out.print("\u23af");
            else
                System.out.print("\uff5c");
        }
        System.out.print("\uff5c");
        System.out.println("");
    }

    public  void print()
    {

        System.out.println("");
        for(int i=0;i<dimension;i++)
        {
            if(i==0)
            {
                this.printLines();

            }
            for(int j=0;j<dimension;j++)
            {
                if(j==0)
                    System.out.print("\t\t \uff5c");
                if(matrix[i][j]==1)
                    System.out.print("\u2660");
                else
                    System.out.print(" ");

                System.out.print("\uff5c");

            }

            this.printLines();


//            System.out.println("");
        }
    }

    public boolean isRegular()
    {
        int neighboursNumber=0;
        for(int i=0;i<dimension;i++)
        {
            int tempCounter=0;
            for(int j=0;j<dimension;j++)
            {
                if(i==0 && matrix[i][j]==1)
                    neighboursNumber++;
                else
                    tempCounter++;
            }
            if(i!=0 && neighboursNumber!= tempCounter)
                return false;
        }

        return true;
    }

    public int nr=0;
    public void nrNoduriComponenta(int start)
    {
        for(int i=0;i<dimension;i++)
            if(matrix[start][i] == 1  && start != i)
            {
                nr++;
//                nrNoduriComponenta(i);
            }

    }
    public int nrConnectedComponents()
    {

        int[] visited = new int[50];
        int counter = 1;
        int p=0;
        int ok=0;
        while(ok==0) {

            ok=1;

            visited[p] = 1;
            int[] coada = new int[50];
            int indexCoada=1;
            coada[0] = p;

            int parcCoada=0;
            while(parcCoada<=indexCoada)
            {
                for(int j=0;j<dimension;j++)
                    if(matrix[coada[parcCoada]][j] ==1 && visited[j]!=1)
                    {
                        visited[j] = 1;
                        coada[indexCoada++] = j;
                    }
                parcCoada ++;
            }

            int i;
            for (i = 0; i < dimension;  i++)
                if(visited[i] != 1)
                {
                    p=i;
                    ok=0;
                    counter++;
                    break;
                }
            else
                ok=1;
                System.out.println(indexCoada );

        }

        return counter;
    }

    private void edgeNumber()
    {
        int counter=0;
        for(int i=0;i<this.dimension;i++)
            for(int j=0;j<this.dimension;j++)
                if(matrix[i][j]==1)
                    counter++;

        edgeCounter = counter;
    }

    private void minMaxDegreez()
    {
        int counter =0;
        minimumDegree=dimension;
        maximumDegree = 0;
        for(int i=0;i<this.dimension;i++)
        {
            counter=0;
            for(int j=0;j<this.dimension;j++)
            {
                if(this.matrix[i][j]==1)
                    counter++;
            }
            if(counter<minimumDegree)
                minimumDegree = counter;
            if(counter>maximumDegree)
                maximumDegree = counter;
        }

    }

}
