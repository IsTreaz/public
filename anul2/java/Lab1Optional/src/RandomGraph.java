public class RandomGraph extends Graph {
    public RandomGraph(int dimension)
    {
        this.dimension = dimension;
        matrix = new int[dimension][dimension];
        for(int i=0;i<dimension;i++)
            for(int j=0;j<dimension;j++)
                matrix[i][j] = 0;


        for(int i=0;i<dimension-1;i++)
        {
            int numberOfRandoms = (int) (Math.random() * dimension);
            for (int j = 0; j < numberOfRandoms/2; j++)
            {
                int muchie = (int) (Math.random() * dimension);
                if(i!=muchie)
                    matrix[i][muchie] = matrix[muchie][i] = 1;
            }

            this.initialize();
        }
    }

}
