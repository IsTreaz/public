public class CompleteGraph extends Graph {


    public CompleteGraph(int n) {
        this.dimension = n;
        matrix = new int[dimension][dimension];
        for(int i=0;i<dimension;i++)
            for(int j=0;j<dimension;j++)
                matrix[i][j] = 0;


        for(int i=0;i<dimension;i++)
            for(int j=0;j<dimension;j++)
                if(i != j)
                    matrix[i][j] = 1;

        this.initialize();
    }

}
