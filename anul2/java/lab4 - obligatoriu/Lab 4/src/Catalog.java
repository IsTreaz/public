import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Catalog implements Serializable {
    private List<Graph> catalogEntries = new ArrayList<Graph>();
    private String filePath;

    public Catalog(String filePath) {
        this.filePath = filePath;
    }

    public void add(Graph graph) {
        catalogEntries.add(graph);
    }

    public void list() {
        for (Graph entry : this.catalogEntries) {
            System.out.println("***\nName: " + entry.name + "\nDefinition:  " + entry.definition + "\nImage: " + entry.image);
        }
    }

    public void open(String name) throws CatalogIO, MiniException {
        File newFile = new File(this.findGraphDefinition(name));
        try {
            Desktop desktop = Desktop.getDesktop();
            if (newFile.exists()) {
                desktop.open(newFile);
            }
        } catch (Exception e) {
            throw new CatalogIO("OPEN EXCEPTION", e.getMessage());
        }
    }


    public void save(String fileName) throws CatalogIO, MiniException {
        try {

            FileOutputStream file = new FileOutputStream(fileName);
            ObjectOutputStream out = new ObjectOutputStream(file);


            out.writeObject(this);

            out.close();
            file.close();

            System.out.println("Object has been serialized");
        } catch (Exception e) {
            throw new MiniException();
        }
    }

    public void load(String fileName) throws CatalogIO, MiniException {
        try {
            FileInputStream file = new FileInputStream(fileName);
            ObjectInputStream in = new ObjectInputStream(file);

            Catalog catalog = (Catalog) in.readObject();

            in.close();
            file.close();

            System.out.println("Object has been deserialized ");
            catalog.list();

        } catch (IOException | ClassNotFoundException ex) {
            throw new MiniException();
        }
    }

    private String findGraphDefinition(String name) throws CatalogIO, MiniException {
            for (Graph graph : catalogEntries) {
                if (graph.name == name) {
                    return this.filePath + graph.image;
                }
            }
         throw new MiniException();
    }

}



