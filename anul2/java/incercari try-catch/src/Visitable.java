import java.time.Duration;
import java.time.LocalTime;

public interface Visitable {
    default void setTime(LocalTime openingTime, LocalTime closingTime)
    {
        openingTime = LocalTime.of(9, 30);
        closingTime = LocalTime.of(20,00);
    }
    static Duration getVisitingDuration(Node n)
    {
        return  Duration.between(n.getOpeningTime(), n.getClosingTime());

    }
}
