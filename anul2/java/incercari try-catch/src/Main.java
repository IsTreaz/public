import javax.xml.stream.Location;
import java.lang.reflect.Array;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Hotel h1 = new Hotel("Unirea");
        Hotel h2 = new Hotel("Traian");
        Hotel h3 = new Hotel("Arabs");



        h1.setTime(LocalTime.of(9, 30), LocalTime.of(21, 30));
        h2.setTime(LocalTime.of(10, 30), LocalTime.of(21, 30));
        h3.setTime(LocalTime.of(10, 00), LocalTime.of(21, 30));

        Duration d  = Visitable.getVisitingDuration(h1);
        System.out.println(d.toHours())  ;

        Museum m1 = new Museum("Cuza Voda");
        Museum m2= new Museum("Stiinge");
        Museum m3 = new Museum("G Toparceanu");
        Museum m4 = new Museum("Eminescu");

        m1.setPrice(15);
        m2.setPrice(20);
        m3.setPrice(25);
        m4.setPrice(30);

        TravelMap travelMap = new TravelMap();

        travelMap.addNode(h1);
        travelMap.addNode(h2);
        travelMap.addNode(h3);
        travelMap.addNode(m1);
        travelMap.addNode(m2);
        travelMap.addNode(m3);
        travelMap.addNode(m4);


        travelMap.addEdge(h1, h2, 15);
        travelMap.addEdge(h1, m1, 10);
        travelMap.addEdge(h2, m1, 30);

        System.out.println("this map contains: " +  travelMap);


        System.out.println("The most eficient road is  .. ");

        System.out.println("Hello World!");

        ArrayList<Node> nodes =  travelMap.getNodes();

        List<Node> node1 = nodes.stream().filter(x->x.isVisitable()==true).filter(x->x.isPayable()==false).collect(Collectors.toList());
        node1.stream().sorted((n1, n2)-> n1.getOpeningTime().compareTo(n2.getOpeningTime()));
        node1.stream().forEach(y-> System.out.print(y + "  "));

        System.out.println("\n\n");


//        nodes.stream().filter(x->x.isPayable() == true).forEach(y-> System.out.println(y));
        System.out.println(nodes.stream().filter(x->x.isPayable() == true).mapToInt((x)->x.getPrice()).average().getAsDouble());

    }
}
