public class Restaurant extends Node implements Classifiable{
    public Restaurant(String name) {
        this.name = name;
    }
    int rank;

    public void setRank(int rank)
    {
        this.rank = rank;
        this.isClassifiable = true;
    }
    public int getRank()
    {
        return this.rank;
    }
}
