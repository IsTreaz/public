import java.time.LocalTime;

public class Hotel extends Node implements Visitable {

    public Hotel(String name) {
        this.name = name;
    }

    public boolean getStatus() {
        return true;
    }

    public void setTime(LocalTime openingTime, LocalTime closingTime)
    {
        this.openingTime = openingTime;
        this.closingTime = closingTime;
        this.isVisitable = true;

    }
}
