public class Museum extends Node implements Payable{
    public Museum(String name) {
        this.name = name;
    }

    public void setPrice(int price)
    {
        this.price = price;
        this.isPayable = true;
    }
}
