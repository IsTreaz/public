import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TravelMap implements Comparator<Node> {

    ArrayList<Node> nodes = new ArrayList<>();
    List<Touple> edges = new ArrayList<>();

    public void addNode(Node node) {
        nodes.add(node);
    }

    public void addEdge(Node node1, Node node2, int price) {
        Touple touple = new Touple(node1, node2, price);
        edges.add(touple);
    }

    public ArrayList<Node> getNodes() {
        return nodes;


    }



    @Override
    public int compare(Node node, Node t1) {
        int l1 = node.name.length();
        int l2 = t1.name.length();
        for (int i = 0; i < l1 && i < l2; i++) {
            int str1 = (int) node.name.charAt(i);
            int str2 = (int) t1.name.charAt(i);

            if (str1 == str2)
                continue;
            else
                return str1 - str2;
        }
        if (l1 < l2)
            return l1 - l2;
        else if (l2 > l1)
            return l2 - l1;
        return 0;

    }


    @Override
    public String toString() {
        StringBuilder nodeNames = new StringBuilder();
//        Collections.sort(nodes);

//
        nodes.sort(this::compare);
        nodeNames.append("\n");
        for (Node node : nodes) {
            nodeNames.append(node.name);
            nodeNames.append("\n");
        }

        return nodeNames.toString();
    }
}
