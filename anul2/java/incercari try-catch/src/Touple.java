public class Touple {
    Node n1;
    Node n2;
    int price;

    public Touple(Node n1, Node n2, int price) {
        this.n1 = n1;
        this.n2 = n2;
        this.price = price;
    }
    public Node getNode1(){
        return n1;
    }
    public Node getNode2()
    {
        return n2;
    }
    public int getPrice()
    {
        return price;
    }
}
