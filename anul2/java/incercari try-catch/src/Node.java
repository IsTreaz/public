import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Comparator;

public abstract class Node implements Comparable<Node> {
    protected String name;
    protected boolean isPayable = false;
    protected boolean isClassifiable = false;
    protected boolean isVisitable = false;

    protected LocalTime openingTime;
    protected LocalTime closingTime;
    protected int rank;
    protected int price;


    public LocalTime getOpeningTime()
    {
        return this.openingTime;
    }
    public LocalTime getClosingTime()
    {
        return this.closingTime;
    }

    public int getPrice()
    {
        return this.price;
    }

    public boolean isPayable()
    {
        return this.isPayable;
    }
    public boolean isClassifiable()
    {
        return this.isClassifiable;
    }
    public boolean isVisitable()
    {
        return this.isVisitable;
    }

    public String getName()
    {
        return this.name;
    }

    @Override
    public String toString() {
        return name + " ";
    }

    @Override
    public int compareTo(Node node) {
        if(this.getOpeningTime().getHour() != node.getOpeningTime().getHour())
            return this.getOpeningTime().getHour() - node.getOpeningTime().getHour();
        else
            return this.getOpeningTime().getMinute() - node.getOpeningTime().getMinute();
    }
}
