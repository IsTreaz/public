package com.company;

import java.time.LocalDate;

public class Application extends Project {
        public enum Language
        {
            rom, eng;
        }
       public Language language;

        public Application(String name, LocalDate date, Language language)
        {
            this.name = name;
            this.date = date;
            this.language = language;
        }
}
