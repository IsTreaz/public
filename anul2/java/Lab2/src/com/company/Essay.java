package com.company;

import java.time.LocalDate;

public class Essay extends Project {
    public enum Topics
    {
        algorithms, data_structures;
    }
    Topics topics;

    public Essay(String name, LocalDate date, Topics topics)
    {
        this.name = name;
        this.date = date;
        this.topics = topics;
    }
}
