package com.company;

import java.time.LocalDate;

public abstract  class Project {
    LocalDate date;
    String name;

    @Override
    public boolean equals(Object obj) {
        Project p = (Project) obj;
        if(p.name == this.name)
            return true;
        return false;
    }
}
