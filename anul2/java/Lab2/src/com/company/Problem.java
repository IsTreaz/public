package com.company;

import java.time.LocalDate;
import java.util.ArrayList;

public abstract class Problem {

    ArrayList<Student> students = new ArrayList<Student>();

    public void setStudents(Student... studs)
    {
        for (int i = 0; i < studs.length; i++) {
            if(students.contains(studs[i])==false)
                students.add(studs[i]);
        }
    }

    public ArrayList<Project> getProjects()
    {
        ArrayList<Project> projects = new ArrayList<Project>();

//        Problem
        for (int i = 0; i < students.size(); i++) {
            Student stud = students.get(i);
            for (int j = 0; j < stud.projects.size(); j++) {
                Project proj = stud.projects.get(j);
                projects.add(proj);
            }
        }
        return projects;
    }


    @Override
    public String toString() {
        return "Studentii sunt: \n" + students;
    }
}

