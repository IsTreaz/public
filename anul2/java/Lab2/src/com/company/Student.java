package com.company;

import java.util.ArrayList;

public class Student {

    private String name;
    private int year;
    public ArrayList<Project> projects ;

    public String getName()
    {
        return this.name;
    }

    public Student(String name, int year)
    {
        this.name = name;
        this.year = year;
        projects = new ArrayList<Project>();
    }

    public void setPreferences(Project... projects)
    {
        for (int i = 0; i < projects.length; i++)
        {
            this.projects.add(projects[i]);
        }

    }
    @Override
    public String toString()
    {
        return "  " + name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == this)
            return true;
        if(!(obj instanceof Student))
            return false;
        Student s = (Student) obj;
        if(this.name == s.name && this.year == s.year)
            return true;
        return false;

    }

}
