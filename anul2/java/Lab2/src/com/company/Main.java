package com.company;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;

public class Main {

    public enum language{
        JAVA, HTML;
    }
    public enum topics{
        ALGORITHMS, DATA_STRUCTURES, WEB, DATABASES;
    }

    public static void main(String[] args) {
        System.out.println("Hello World!");



        Student s1 = new Student("S1", 2);
        Student s2 = new Student("S2", 3);
        Student s3 = new Student("S3", 1);
        Student s4 = new Student ("S4", 2);
        Student s5 = new Student("S5", 1);

        Application a1 = new Application("a1", LocalDate.of(2019, Month.JUNE, 1), Application.Language.rom);
        Application a2 = new Application("a2", LocalDate.of(2019, Month.JULY, 2), Application.Language.eng);

        Essay e1 = new Essay("e1", LocalDate.parse("2019-06-01"), Essay.Topics.algorithms);
        Essay e2 = new Essay("e2", LocalDate.parse("2019-07-02"), Essay.Topics.algorithms);

        s1.setPreferences(e1, a1, a2, e2);
        s2.setPreferences(a1);
        s3.setPreferences(e1, e2);
        s4.setPreferences(a2, e2);
        s5.setPreferences(a1, a2, e1, e2);

        Problem problem = new Problem() {};
        problem.setStudents(s1,s2,s3,s1);
        System.out.println(problem);

        ArrayList<Project> projects = new ArrayList<>();
        projects = problem.getProjects();

        System.out.println("");
        for (int i = 0; i < projects.size(); i++)
        {
            System.out.print(projects.get(i).name + " ");
        }
        System.out.println(" ");
        Match match = new Match();
        match.getMatching(problem);

    }
}
