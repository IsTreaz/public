package com.company;

import java.util.ArrayList;
import java.util.List;

public class Match {

    public void getMatching( Problem problem)
    {
        List<Project> assignedProjects = new ArrayList<>();

        for (Student std : problem.students)
        {
            for (Project project : std.projects)
            {
                if(assignedProjects.contains(project)==false)
                {
                    assignedProjects.add(project);
                    System.out.println("Studentului " + std.getName() + " ii atribuim proiectul " + project.name);
                    break;
                }
            }
        }
    }
}
