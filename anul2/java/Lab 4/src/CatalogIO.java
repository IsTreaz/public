
public class CatalogIO extends Exception {
    private String name;
    private String description;

    public CatalogIO(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String getMessage(){
        return this.name+"\n"+this.description;
    }
}
