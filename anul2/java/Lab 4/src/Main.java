public class Main {

    public static void main(String[] args) throws CatalogIO {
        Catalog catalog = new Catalog("Fisiere/");
        catalog.add (new Graph("K4", "TGF-uri/k4.tgf", "/home/eduard/Documents/public/java/Lab 4/Fisiere/TGF-uri/view/k4.png"));
        catalog.add (new Graph("Petersen", "TGF-uri/petersen.tgf", "/home/eduard/Documents/public/java/Lab 4/Fisiere/TGF-uri/view/petersen.png"));

        catalog.open("Petersen");
        catalog.open("K4");

        catalog.save("catalog.dat");
        catalog.load("catalog.dat");

        catalog.list();
    }
}
