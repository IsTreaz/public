public class SaveCommand implements CommandLine {
    @Override
    public void run(Catalog c, String name) {
        c.save(name);
    }
}
