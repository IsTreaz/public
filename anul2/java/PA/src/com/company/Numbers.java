package com.company;

import java.util.ArrayList;

public class Numbers {
    public int number;
    //    public String[] limbaje;
    public ArrayList<String> limbaje;
    public int binary, hexa;

    Numbers()
    {
        this.number = (int) (Math.random() * 1_000_000);
        limbaje = new ArrayList<String>() {{
            add("C");
            add("C++");
            add("C#");
            add("Go");
            add("JavaScript");
            add("Perl");
            add("PHP");
            add("Python");
            add("Swift");
            add("Java");
        }};

        binary = Integer.parseInt("10101", 2);
        hexa = Integer.parseInt("FF", 16);
    }

    public void add(){
        number *= 3;
        number = number + binary + hexa;
        number *= 6;
    }

    public void digitSum()
    {
        while(this.number>9)
        {
            this.number = this.number/10 + this.number%10;
        }
    }

}
