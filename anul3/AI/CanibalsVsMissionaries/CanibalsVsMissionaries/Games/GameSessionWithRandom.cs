﻿using System;
using System.Collections.Generic;

namespace CannibalsVsMissionaries
{
    public class GameSessionWithRandom : GameSession
    {

        public GameSessionWithRandom(int missionaries, int cannibals, int transportCapacity)
        {
            this.transportCapacity = transportCapacity;
            ActualState = new State(missionaries, cannibals);
        }

        public override void Play()
        {
            int blockedCount = 0;
            int maxBlockedCount = 0;
            int invalidStateIndex = 0;
            int iterationIndex = 0;
            OldStates.Add(ActualState);
            while (true)
            {
                iterationIndex++;
                var boatGroup = generateRandomGroup();

                var transitionState = new State(ActualState);

                transitionState.Transition(boatGroup.Missionaries, boatGroup.Cannibals);

                if (transitionState.IsValid())
                {
                    if (OldStates.Contains(transitionState))
                    {
                        blockedCount++;
                        //if (maxBlockedCount < blockedCount)
                        //{
                        //    maxBlockedCount = blockedCount;
                        //}
                    }
                    else
                    {
                        blockedCount = 0;
                        OldStates.Add(transitionState);
                    }
                    ActualState = transitionState;

                    if (blockedCount == 100)
                    {
                        ActualState = OldStates[0];
                        OldStates = new List<State>();
                        OldStates.Add(ActualState);

                        Console.WriteLine("found 100");
                    }

                }
                else
                {
                    invalidStateIndex++;
                    if (invalidStateIndex % 3500 == 0)
                    {

                        ActualState = OldStates[0];
                        OldStates = new List<State>();
                        OldStates.Add(ActualState);

                        Console.WriteLine("100 mistakes");
                    }
                }
                if (ActualState.IsFinished())
                {
                    Console.WriteLine("Am gasit o solutie buna");
                    Console.WriteLine("Avem un total de: " + iterationIndex + " iteratii");
                    Console.WriteLine("Dintre care un total de: " + invalidStateIndex + " stari invalide");
                    Console.WriteLine("Cel m-ai mult m-am repetat de: " + maxBlockedCount + " ori");
                    Console.WriteLine(ActualState.ToString());
                    return;
                }
            }
        }

        private Group generateRandomGroup()
        {
            var group = new Group();

            group.Missionaries = _random.Next() % (transportCapacity + 1);
            group.Cannibals = _random.Next() % (transportCapacity + 1);


            if (group.Missionaries + group.Cannibals == 0 || (group.Missionaries + group.Cannibals) > transportCapacity)
            {
                return generateRandomGroup();
            }
            return group;
        }


    }
}