﻿using System;
using System.Collections.Generic;

namespace CannibalsVsMissionaries
{
    public class GameSessionWithBkt : GameSession
    {
        public GameSessionWithBkt(int missionaries, int cannibals, int transportCapacity)
        {
            this.transportCapacity = transportCapacity;
            ActualState = new State(missionaries, cannibals);
        }

        public override void Play()
        {
            BktIteration(ActualState);
        }

        private void BktIteration(State state)
        {
            if (state.IsFinished())
            {
                Console.WriteLine("Bkt Algorithm found an solution");
                Console.WriteLine();
            }
            else
            {
                var allStates = GetAllPosibilities(state);
                foreach (var newState in allStates)
                {
                    if (!OldStates.Contains(newState))
                    {
                        OldStates.Add(newState);
                        BktIteration(newState);
                    }
                }
            }
        }
        private List<State> GetAllPosibilities(State state)
        {
            var list = new List<State>();
            for (var i = 0; i < transportCapacity; i++)
                for (var j = 0; j < transportCapacity; j++)
                {
                    var newState = new State(state);
                    newState.Transition(i, j);
                    if (newState.IsValid())
                        list.Add(newState);
                }
            return list;
        }
    }
}