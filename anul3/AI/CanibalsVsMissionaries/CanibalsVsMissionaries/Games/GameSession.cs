﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace CannibalsVsMissionaries
{
    public abstract class GameSession
    {
        protected State ActualState;
        protected List<State> OldStates = new List<State>();

        protected int transportCapacity = 0;
        protected Random _random = new Random();

        public abstract void Play();
    }
}