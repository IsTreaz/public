namespace CannibalsVsMissionaries
{
    public class Group
    {
        public int Missionaries;
        public int Cannibals;

        public Group()
        {
            Cannibals = 0;
            Missionaries = 0;
        }

        public bool IsValid()
        {
            if (Missionaries < 0 || Cannibals < 0)
            {
                return false;
            }

            if (Missionaries == 0)
            {
                return true;
            }

            return Missionaries >= Cannibals;
        }

        public void Add(int missionaries, int cannibals)
        {
            Cannibals += cannibals;
            Missionaries += missionaries;
        }

        public void Remove(int missionaries, int cannibals)
        {
            Cannibals -= cannibals;
            Missionaries -= missionaries;
        }

        public override string ToString()
        {
            return "Missionaries: " + this.Missionaries + " cannibals: " + this.Cannibals;
        }
    }
}