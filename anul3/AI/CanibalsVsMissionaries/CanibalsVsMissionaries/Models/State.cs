using System;
using System.Collections.Generic;
using System.Data.SqlTypes;

namespace CannibalsVsMissionaries
{
    public class State
    {
        public Side Left;
        public Side Right;

        public int CannibalsNumber => Left.hasBoat? Left.Cannibals : Right.Cannibals;
        public int MissionariesNumber => Left.hasBoat? Left.Missionaries : Right.Missionaries;

        public State(int missionaries, int cannibals)
        {
            Left = new Side(true, missionaries, cannibals);
            Right = new Side(false, 0, 0);            
        }
        public State(State state)
        {
            this.Left = new Side(state.Left.hasBoat, state.Left.Missionaries, state.Left.Cannibals);
            this.Right = new Side(state.Right.hasBoat, state.Right.Missionaries, state.Right.Cannibals);
        }

        public void Transition(int missionaries,  int cannibals)
        {
            if (this.Left.hasBoat)
            {
                // left to right
                Left.Remove(missionaries, cannibals);
                Right.Add(missionaries, cannibals);

                Left.hasBoat = false;
                Right.hasBoat = true;
            }
            else
            {
                //right to left
                Left.Add(missionaries, cannibals);
                Right.Remove(missionaries, cannibals);

                Left.hasBoat = true;
                Right.hasBoat = false;
            }

        }

        public bool IsValid()
        {
            return Left.IsValid() && Right.IsValid();
        }

        public bool IsFinished()
        {
            return Left.Cannibals == 0 && Left.Missionaries == 0;
        }

        public override string ToString()
        {
            return "Left " + Left.Cannibals + " " + Left.Missionaries + "\n" + "Right " + Right.Cannibals + " " + Right.Missionaries; 
        }

        public override bool Equals(object obj)
        {
            var state = (State)obj;
            return state.Left.Equals(Left) && state.Right.Equals(Right);
        }
    }

    public class IndexedState
    {
        public State state;
        public int count;
        public IndexedState(State state, int count)
        {
            this.state = state;
            this.count = count;
        }

    }
}

// capacitatea barcii este data ca input
// barca sa nu transporte 0
