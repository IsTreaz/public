﻿namespace CannibalsVsMissionaries
{
    public class Side : Group
    {
        public bool hasBoat;

        public Side(bool hasBoat, int missionaries, int cannibals)
        {
            this.hasBoat = hasBoat;
            Cannibals = cannibals;
            Missionaries = missionaries;
        }

        public override bool Equals(object obj)
        {
            return obj is Side side &&
                   hasBoat == side.hasBoat &&
                  this.Cannibals == side.Cannibals &&
                  this.Missionaries == side.Missionaries;
        }
    }
}