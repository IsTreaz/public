using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CannibalsVsMissionaries
{
    class Program
    {
        static void Main(string[] args)
        {
            var BktGame = new GameSessionWithBkt(5, 5, 3);
            BktGame.Play();

            var RandomGame = new GameSessionWithRandom(5, 5, 3);
            RandomGame.Play();
        }
    }
}
