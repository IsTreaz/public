def fibo(n):
    a = 1
    b = 1
    if n < 3:
        return 1
    n = n - 2

    while n > 0:
        c = a + b
        a = b
        b = c
        n -= 1
    return b


def is_fibo(n):
    a = 1
    b = 1
    if n is 1:
        return True
    while b < n:
        c = a + b
        a = b
        b = c
    if n == b:
        return True
    return False


def is_prime(number):
    if number is 1:
        return False
    d = 2
    val = number ** 0.5

    while d < val:
        if number % d is 0:
            return False
        d += 1
        if d % 2 is 0:
            d += 1
    return True


def custom_filter(my_list):
    result = []
    for item in my_list:
        if is_prime(item) is True and is_fibo(item) is True:
            result.append(item)
    result.sort()
    return result


def get_odds(l):
    result = []
    for item in l:
        if item % 2 == 0:
            result.append(item)
    return result


def extract_numbers(text):
    digits = "0123456789"
    result = []
    number = -1
    for char in text:
        if number == -1 and char in digits:
            number = int(char)
        elif number is not -1 and char in digits:
            number = number * 10 + int(char)
        if number != -1 and char not in digits:
            result.append(number)
            number = -1

    if number != -1:
        result.append(number)

    result.sort(reverse=True)
    return result


def special_sum(*strings):
    s = 0
    for string in strings:
        numbers = extract_numbers(string)
        number = get_odds(numbers)
        number = number[0]
        s = s + number
    return s


def loop(mapping):
    a = {}
    # a.keys()
    result = []
    start = "start"
    v = mapping[start]
    result.append(v)
    while v is not "start" or result.count == list(mapping.keys()).count:
        start = v
        v = mapping[start]
        if v is not "start":
            if v in result:
                return result
            result.append(v)

    return result


solutions = {}


def sequence(n):
    if n == 0:
        return 2
    if n == 1:
        return 4

    result = 2 * sequence(int(n / 2)) - sequence(n - 2)
    solutions.update({n: result})
    return result


# print(loop({"start": "3", "3": "1", "1": "2", "2": "start"}))
# print(special_sum("a2b5c4", "x8y9z10"))


# print(extract_numbers("test12z34p5"))

# print(is_prime(1597))
# print(is_fibo(1597))

# my_list = [610, 377, 987, 4181, 2584, 89, 144, 1597, 55, 6765, 233]
# print(custom_filter(my_list))
# print(is_prime(1713371921))


# print(fibo(10))
