def isPrime(number):
    if number < 4: 
        return True
    divizor = 2
    while(divizor < number):
        if number % divizor == 0:
            return False
        divizor += 1
    return True

def checkList(numbers):
    result = []
    for number in numbers:
        if isPrime(number):
            result.append(number)
    return result


string = input()
array = string.split()
numbers = [int(element) for element in array]
# numbers = list(map(int, list))

result = checkList(numbers)
print( result) 