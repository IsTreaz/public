def fibo(size):
    list = []
    first = 1
    second = 1
    if size < 3:
        return 1
    i = 2
    while (i <= size):
        third = first + second
        first = second
        second = third
        i = i+1
    return third
size = int(input())
result = fibo(size)
print(result)