def getElems(countSize, *lists):
    bigList = sum(lists, [])
    result = []
    for number in bigList:
        if bigList.count(number) == countSize:
            if number not in result:
                result.append(number)
    return result