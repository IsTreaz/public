def intersection(list1, list2):
    return [value for value in list1 if value in list2]

def reunion(list1, list2):
    return list(set(list1) | set(list2))

def minus(list1, list2):
    return list(set(list1) - set(list2))


list2 = [4,5,6,7,8]
list1 = [1,2,3,4,5,6,7]
# print (set(list1) | set(list2))
print (intersection(list1, list2))
print (reunion(list1, list2))
print (minus(list1, list2))