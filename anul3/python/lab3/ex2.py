def createDict(str):
    result = {}
    letters = set(str)
    for letter in letters:
        result.setdefault(letter, str.count(letter))
    return result

str = " mama are mere"
print(createDict(str))