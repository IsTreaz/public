def duplicateAndDiffNumber(setObj):
    duplicateCount = 0
    diffCount = 0
    objects = list(setObj) 
    for item in objects:
        if objects.count(item)==1:
            diffCount +=1
        else:
            duplicateCount +=1
    return {duplicateCount, diffCount}


a = [1,2,3,4,5,5,6,6,7,8]
c = duplicateAndDiffNumber(set(a))
print(c)
