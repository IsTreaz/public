def validateDict(dict, rule):
    # ''.endswith
    for key in dict.keys():
        if not key.startswith(rule[0]): 
           return False
        if not key.endswith(rule[2]):
            return False
        if not key.find(rule[1]):
            return False
    return True


dict = {
    "balara": "xaxa"
}
rule = ("b", "ala", "r")
print(validateDict(dict, rule))