def compareDict(dict1, dict2):
    diffKeys = list(set(dict1.keys()).symmetric_difference(set(dict2.keys())))
    commonKeys = list(set(dict1.keys()).intersection(set(dict2.keys())))
    for key in commonKeys:
        
        if isinstance(dict1[key], dict) and isinstance(dict2[key], dict):
            newDiffkeys = compareDict(dict1[key], dict2[key])
            diffKeys = list(set(diffKeys) + set(newDiffkeys))
    return diffKeys

dict1 = {
    "ana": 4,
    "maria": 7,
    "PBC":{
        "rara":0,
        "gara":0
    }
}

dict2 = {
    "ana": 4,
    "mariaa": 7,
}

print(compareDict(dict1, dict2))
