list1 = [1,2,3,4,5,6]
list2 = [4,5,6,7,8]
def intersect(l1, l2):
    return [value for value in l1 if value in l2]
def reunion(l1,l2):
    return list( set(l1) | set(l2))
def minus(l1,l2):
    return list(set(l1) - set(l2))

print(intersect(list1, list2))
print(reunion(list1, list2))
print(minus(list1, list2))