dict1 = {    

    "+": lambda a, b: a + b,     

    "*": lambda a, b: a * b,

    "/": lambda a, b: a / b,

    "%": lambda a, b: a % b
}

# dict1['$']= lambda a,b: a*2+b

def apply(op, a, b):
    if op in dict1.keys():
        return dict1[op](a,b)

a = apply('+', 5,4)
print(a)