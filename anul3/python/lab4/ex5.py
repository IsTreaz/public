def fn(*dicts):
    result = {}
    for d in dicts:
        for key in d.keys():
            if key in result.keys():
                if not isinstance(result[key], list):
                    result.update({key: [result[key]]})
                result[key].append(d[key])
            if key not in result.keys():
                result.update({key :d[key]})
    return result

d1 = {
    "A":3,
    "B": {
        "E": 7
    }
}
d2 = {
    "B": 6,
    "D":8,
    "E": 8
}
d3 = {
    "B" : 3
}
print(set(d1))
x = fn(d1,d2, d3)
print(x)