dict1 = {

    "print_all": lambda *a, **k: print(a, k),

    "print_args_commas": lambda *a, **k: print(a, k, sep=", "),

    "print_only_args": lambda *a, **k: print(a),

    "print_only_kwargs": lambda *a, **k: print(k)

}

def apply(op, *a, **k):
    if op in dict1.keys():
        dict1[op](*a,**k)

a = [1,2,3,4]
k = { "key1" : 1, "key2": 2}
apply("print_all", *a, **k)