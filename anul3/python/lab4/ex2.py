touples = [
    ('George', 'Amariei'),
    ('Teo', 'Calare'),
    ('Aulian', 'Calutu'),
    ('Cosmin', 'Serban'),
    ('Margareta', 'Train'),
    ('Maria', 'Popescu'),
    ('Teodora', 'Georgescu'),
]

def find(tpls, s):
    for t in tpls:
        if s==t[1]:
            return True
    return False

find(touples, "Calutuu")