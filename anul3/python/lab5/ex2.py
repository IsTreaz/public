def fn(*a, **kargs):
    s = 0
    for nr in kargs.values():
        s+= nr
    return s

def addElems(a):
    s =0
    for nr in a:
        s+=nr
    return s

func = lambda *a, **kargs: addElems(kargs.values())
x = func(1,2,c=3,d=4)
# x = fn()
print(x)