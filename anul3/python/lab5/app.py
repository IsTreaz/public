import utils
data_input = input()
while data_input is not 'q':
    number = int(data_input)
    print(utils.process_item(number))
    data_input = input()
