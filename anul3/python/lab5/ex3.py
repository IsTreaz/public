def getVowels(string):
    vowels = "aeiou"
    result = []
    for letter in string:
        if letter in vowels:
            result.append(letter)
    print(result)

def isVowel(letter):
    vowels = "aeiou"
    return letter in vowels

string = "ana are mere"
vowels = "aeiou"
result = [i  for i in string if i in vowels ] # list compre
get = lambda string: getVowels(string)
filterResult = list(filter(isVowel, string))
lmd = lambda string: list(filter(isVowel, list(string)))

print(lmd(string))
# getVowels("Programming in Python is fun")
# str = "alalal"
# str.