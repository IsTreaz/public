def problem1(my_list):
    def is_odd(nr):
        if nr % 2 == 1:
            return True
        return False

    def is_even(nr):
        if nr % 2 == 0:
            return True
        return False

    odds = list(filter(is_odd, my_list))
    evens = list(filter(is_even, my_list))

    result = list(zip(evens, odds))
    # result = []
    # for i in range(0, odds.count):
    #     result.append((evens[i], odds[i]))
    return result


def problem2(pairs):
    result = []

    for touple in pairs:
        result.append({
            "sum": touple[0]+touple[1],
            "prod": touple[0] * touple[1],
            "pow": pow(touple[0], touple[1])
        })

    return result

pairs = [(5, 2), (19, 1), (30, 6), (2, 2)]
# x = [(2, 3), (5, 6)]
c = problem2(pairs);
print(c)
