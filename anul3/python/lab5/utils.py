def is_prime(x):
    if x < 2:
        return False
    for d in range(2, x - 2):
        if x % d == 0:
            return False
    return True


def process_item(number):
    number += 1
    while True:
        if is_prime(number):
            return number
        number += 1


if __name__ == "__main__":
    nr = int(input())
    print(process_item(nr))
else:
        print(__name__)

