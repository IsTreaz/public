// let m = 1;
// while (1 + Math.pow(10, -m) != 1) {
//     m++;
// }
// m--;
// powResult = Math.pow(10, -m);
// console.log(`The smallest u number is: ${powResult} while m = ${m}`);

// x = 1.0
// let y = powResult;
// let z = powResult;

// let first = (x + y) + z;
// let second = x + (y + z);

// first != second ? console.log(`Adunarea nu este asociativa`) : '';

// let x = 1.0;
// first = (x * y) * z;
// second = x * (y * z);
// while (first == second) {
//     x += 1.0;
//     first = (x * y) * z;
//     second = x * (y * z);
// }
// console.log(first, second);

// first != second ? console.log(`Inmultirea nu este asociativa`) : '';

// #####################  ex 3

const A = [
  [1, 0, 1, 1],
  [1, 0, 1, 1],
  [1, 0, 1, 1],
  [0, 1, 1, 0]
];
const B = [
  [1, 0, 1, 0],
  [1, 0, 0, 0],
  [0, 0, 0, 1],
  [0, 0, 0, 0]
];
function add(x, y) {
  if (x === 0 && y === 0) {
    return 0;
  }
  return 1;
}

function getBaseLog(x, base) {
  return Math.log(x) / Math.log(base);
}

function addArrays(arr1, arr2) {
  if (arr1 === undefined) {
    arr1 = [0, 0, 0, 0];
  }
  result = [];

  arr1.forEach((item, index) => {
    result.push(add(item, arr2[index]));
  });

  return result;
}

function getCol(matrix, index) {
  let result = [];
  for (let i = 0; i < matrix.length; i++) {
    result.push(matrix[i][index]);
  }

  return result;
}

function getLine(matrix, index) {
  let result = [];

  for (let i = 0; i < matrix[0].length; i++) {
    result.push(matrix[index][i]);
  }

  return result;
}

function revertMat(mat) {
  result = [];
  len = mat.length;

  for (let i = 0; i < mat[0].length; i++) {
    result.push(new Array());

    for (let j = 0; j < len; j++) {
      result[result.length - 1].push(mat[j][i]);
    }
  }
  return result;
}

function getSubMat(start, final, mat, getFn) {
  let result = [];
  for (let i = start; i < final; i++) {
    const col = getFn(mat, i);
    result.push(col);
    // result.push(new Array());
    // result[i-start].push(col);
  }
  return result;
}

let n = A.length;
// console.log(n);

let m = Math.floor(getBaseLog(n, 2));
// console.log(m);

let p = Math.floor(n / m);
// console.log(p);

let subA = [];
let subB = [];
for (let i = 1; i <= p; i++) {
  let subMat = getSubMat(m * (i - 1), m * i, A, getCol);
  subA.push(revertMat(subMat));
  subMat = getSubMat(m * (i - 1), m * i, B, getLine);
  subB.push(subMat);
}

console.log(subB[0]);

// console.table(subB);
function getK(j) {
  k = 1;
  while (Math.pow(2, k) <= j) {
    k++;
  }

  k--;
  return k;
}

function NUM(arr) {
  result = 0;
  for (let i = 0; i < arr.length; i++) {
    result += Math.pow(2, i) * arr[i];
  }
  return result;
}

let liniiB = [];

for (let i = 0; i < p; i++) {
  sumLiniiB = [];
  sumLiniiB[0] = [0, 0, 0, 0];

  for (let j = 1; j < Math.pow(2, m); j++) {
    const k = getK(j);

    let Bline = getLine(subB[i], k);

    rez = addArrays(sumLiniiB[j - Math.pow(2, k)], Bline);

    sumLiniiB[j] = rez;
  }

  liniiB.push(sumLiniiB);

  // break;
}
let C = [];

for (let i = 0; i < p; i++) {
  for (let x = 0; x < n; x++) {
    C[i] = liniiB[NUM(subA[i][x]) - 1];
  }
}
console.log(C[2][0]);

let result2 = [];

for (let i = 0; i < C.length; i++) {
  result2[i] = [0, 0, 0, 0];
  for (let j = 0; j < 4; j++) {
    console.log(result2[i]);
    console.log(C[i][j]);

    result2[j] = addArrays(result2[j], C[i][j]);
  }
}

console.log(result2);

// console.log(liniiB);

// console.log(getK(4));
// console.table(A)
// console.table(subA[0])
// console.table(subA[1])
// console.table(subB[0]);
// console.log(subA);
