﻿using Microsoft.EntityFrameworkCore;

namespace lab05
{
    public class ProductContext : DbContext
    {
        public DbSet<Product> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().ToTable("Products");
            modelBuilder.Entity<Product>(
                p =>
                {
                    p.Property(e => e.ProductId);
                    p.Property(e => e.ProductTitle);
                    p.Property(e => e.ProductPrice);
                });
            modelBuilder.Entity<Product>().HasData(
                ProductFactory.GetProduct("paine"), 
                ProductFactory.GetProduct("mere"),
                ProductFactory.GetProduct("pere")
                );
        }
    }
}
