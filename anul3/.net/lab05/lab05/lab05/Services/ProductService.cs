﻿using System;
using System.Collections.Generic;

namespace lab05.Repositories
{
    public class ProductService : IProductService
    {
        IProductRepository productRepository = new ProductRepository();
        public void Create(Product product)
        {
            productRepository.Create(product);
        }

        public void Delete(Guid id)
        {
            productRepository.Delete(id);
        }

        public IReadOnlyList<Product> GetAll()
        {
            return productRepository.GetAll();
        }

        public IReadOnlyList<Product> GetAllWithPriceBetween(int lowestPrice, int highestPrice)
        {

            if (lowestPrice <= highestPrice)
            {
                return productRepository.GetAllWithPriceBetween(lowestPrice, highestPrice);
            } else
            {
                return new List<Product>();
            }
        }

        public IReadOnlyList<Product> GetAllWithPriceHigherThan(int price)
        {
            return productRepository.GetAllWithPriceHigherThan(price);
        }

        public Product GetById(Guid id)
        {
            return productRepository.GetById(id);
        }

        public void Update(Guid id, Product product)
        {
            productRepository.Update(id, product);
        }
    }
}
