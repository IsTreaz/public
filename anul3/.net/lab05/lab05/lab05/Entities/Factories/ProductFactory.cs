﻿using System.Collections.Generic;

namespace lab05
{
    public static class ProductFactory
    {
        public static Product GetProduct(string title)
        {
            return new Product(title, 2, 1);
        }

        public static List<Product> GetProducts()
        {
            //return Enumerable.Repeat(GetProduct(), 10).ToList();
            return null;
        }
    }
}
