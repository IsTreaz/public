﻿using System;

namespace lab05
{
    public class Product
    {
        public Product(string title, float price)
        {
            ProductId = Guid.NewGuid();
            ProductTitle = title;
            ProductPrice = price;
        }
        public string ProductTitle { get; private set; }

        public Guid ProductId { get; private set; }

        public float ProductPrice { get; private set; }

    }
}
