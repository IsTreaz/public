﻿using System;
using System.Collections.Generic;

namespace lab05.Repositories
{
    interface IProductRepository
    {
        IReadOnlyList<Product> GetAll();

        IReadOnlyList<Product> GetAllWithPriceHigherThan(int price);

        IReadOnlyList<Product> GetAllWithPriceBetween(int lowestPrice, int highestPrice);

        Product GetById(Guid id);

        void Create(Product product);

        void Update(Guid id, Product product);

        void Delete(Guid id);

    }
}
