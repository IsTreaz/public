﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace lab05.Repositories
{
    public class ProductRepository : IProductRepository
    {
        ProductContext productContext = new ProductContext();

        public void Create(Product product)
        {
            using (ProductContext productContext = new ProductContext())
            {
                productContext.Products.Add(product);

            }
            SaveChanges();
        }

        public void Delete(Guid id)
        {
            using (ProductContext productContext = new ProductContext())
            {

                var product = productContext.Products.First(x => x.ProductId == id);
                productContext.Products.Remove(product);
            }
            SaveChanges();
        }

        public IReadOnlyList<Product> GetAll()
        {
            using (ProductContext productContext = new ProductContext())
            {
                return productContext.Products.ToList();
            }
        }

        public IReadOnlyList<Product> GetAllWithPriceBetween(int lowestPrice, int highestPrice)
        {
            using (ProductContext productContext = new ProductContext())
            {
                return productContext.Products
                .Where(x => x.ProductPrice >= lowestPrice && x.ProductPrice <= highestPrice)
                .ToList();
            }
        }

        public IReadOnlyList<Product> GetAllWithPriceHigherThan(int price)
        {
            using (ProductContext productContext = new ProductContext())
            {
                return productContext.Products
                .Where(x => x.ProductPrice >= price)
                .ToList();
            }
        }

        public Product GetById(Guid id)
        {
            using (ProductContext productContext = new ProductContext())
            {
                return productContext.Products.First(x => x.ProductId == id);
            }
        }

        public void SaveChanges()
        {
            using (ProductContext productContext = new ProductContext())
            {
                productContext.SaveChanges();
            }
        }

        public void Update(Guid id, Product product)
        {
            using (ProductContext productContext = new ProductContext())
            {
                var dbProduct = GetById(id);
                productContext.Products.Update(dbProduct);
            }

            SaveChanges();
        }
    }
}
