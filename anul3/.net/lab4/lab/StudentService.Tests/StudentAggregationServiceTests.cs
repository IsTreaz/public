﻿using System.Linq;
using Xunit;

namespace StudentService.Tests
{
    public class StudentAggregationServiceTests
    {
        StudentAggregationService studentAggregationService = new StudentAggregationService();

        [Fact]
        public void GivenUnion_WhenTwoArraysAreGiven_ThenShouldReturnAggregatedArrays()
        {
            // arrange

            var initialStudents = StudentFactory.Create10Students();
            var studentsToAggregate = StudentFactory.Create5Students();
            var expected = initialStudents.Union(studentsToAggregate).ToArray();

            // act
            var result = studentAggregationService.Union(initialStudents, studentsToAggregate);

            // assert
            Assert.Equal(result, expected);
        }
    }
}
