using System;
using System.Linq;
using Xunit;

namespace StudentService.Tests
{
    public class StudentMethodServiceTests
    {
        StudentMethodService studentMethodService = new StudentMethodService();

        [Fact]
        public void GivenMehodForHigherAge_WhenAgeIsSet_ThenShouldReturnAllStudentsWithHigherAge()
        {
            // arrange
            var age = 19;
            var expected = StudentFactory.Create10Students().Where(x => x.Age > age).ToArray();

            // act
            var result = studentMethodService.GetStudentsWithAgeHigherThatGivenAge(StudentFactory.Create10Students(), age);

            // assert
            Assert.Equal(result, expected);

        }

        [Fact]
        public void GivenMehodForHigherAge_WhenAgeIsNegative_ThenShouldReturnTheInitialArray()
        {
            // arrange
            var age = -1;
            var expected = StudentFactory.Create10Students();

            // act
            var result = studentMethodService.GetStudentsWithAgeHigherThatGivenAge(StudentFactory.Create10Students(), age);

            // assert
            Assert.Equal(result, expected);

        }

        [Fact]
        public void GivenMehodForHigherAge_WhenAgeIsNegative_ThenShouldReturnAnEmptyArray()
        {
            // arrange
            var age = 500;
            var expected = Array.Empty<Student>();

            // act
            var result = studentMethodService.GetStudentsWithAgeHigherThatGivenAge(StudentFactory.Create10Students(), age);

            // assert
            Assert.Equal(result, expected);

        }

        [Fact]
        public void GivenMethodForFilterAgeBetweenYears_WhenBothAgesAreValid_ThenShouldReturnTheFilteredArray()
        {
            // arrange
            var lowerAge = 10;
            var higherAge = 50;
            var expected = StudentFactory.Create10Students().Where(x => x.Age > lowerAge && x.Age < higherAge).ToArray();

            // act
            var result = studentMethodService.GetStudentsWithAgeBetweenGivenYears(StudentFactory.Create10Students(), lowerAge, higherAge);

            // assert
            Assert.Equal(result, expected);
        }

        [Fact]
        public void GivenMethodForFilterAgeBetweenYears_WhenTheLowerAgeIsBiggerThanHigherAge_ThenShouldReturnAnEmptyArray()
        {
            // arrange
            var lowerAge = 50;
            var higherAge = 10;
            var expected = Array.Empty<Student>();

            // act
            var result = studentMethodService.GetStudentsWithAgeBetweenGivenYears(StudentFactory.Create10Students(), lowerAge, higherAge);

            // assert
            Assert.Equal(result, expected);
        }

        [Fact]
        public void GivenMethodForFilterAgeBetweenYears_WhenTheLowerAgeIsNegativeAndHigherAgeIsTooBig_ThenShouldReturnInitialArray()
        {
            // arrange
            var lowerAge = -1;
            var higherAge = 500;
            var expected = StudentFactory.Create10Students();

            // act
            var result = studentMethodService.GetStudentsWithAgeBetweenGivenYears(StudentFactory.Create10Students(), lowerAge, higherAge);

            // assert
            Assert.Equal(result, expected);
        }
   
    
        [Fact]
        public void GivenMethodForCountingNamesByLetters_WhenALetterIsGiven_ThenShouldReturnPropperCount()
        {
            // arrange
            var letter = 'I';
            var expected = StudentFactory.Create10Students().Where(x => x.StudentName.Contains(letter)).Count();

            // act
            var result = studentMethodService.GetNumberOfStudentsThatContainALetter(StudentFactory.Create10Students(), letter);

            // assert
            Assert.Equal(result, expected);
        }
    }
}
