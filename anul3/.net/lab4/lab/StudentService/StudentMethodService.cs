﻿using System.Linq;

namespace StudentService
{
    public class StudentMethodService
    {
        public Student[] GetStudentsWithAgeHigherThatGivenAge (Student[] students, int age)
        {

            return students.Where(x => x.Age > age).ToArray();
        }

        public Student[] GetStudentsWithAgeBetweenGivenYears (Student[] students, int lowerAge, int higherAge)
        {
            return students.Where(x => x.Age > lowerAge && x.Age < higherAge).ToArray();
        }

        public int GetNumberOfStudentsThatContainALetter(Student[] students, char letter)
        {
            return students.Where(x => x.StudentName.Contains(letter)).Count();
        }
    }

}
