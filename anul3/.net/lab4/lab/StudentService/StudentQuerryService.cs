﻿using System.Linq;

namespace StudentService
{
    public class StudentQuerryService
    {
        public Student[] GetStudentsWithAgeHigherThatGivenAge(Student[] students, int age)
        {
            var filteredStudents = from s in students
                                   where s.Age > age
                                   select s;
            return filteredStudents.ToArray();
        }

        public Student[] GetStudentsWithAgeBetweenGivenYears(Student[] students, int lowerAge, int higherAge)
        {
            var filteredStudents = from s in students
                                   where s.Age > lowerAge && s.Age < higherAge
                                   select s;
            return filteredStudents.ToArray();
        }

        public int GetNumberOfStudentsThatContainALetter(Student[] students, char letter)
        {
            var filteredStudents = from s in students
                                   where s.StudentName.Contains(letter)
                                   select s;
            return filteredStudents.Count();
        }
    }
}
