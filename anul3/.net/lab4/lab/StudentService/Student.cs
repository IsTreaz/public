﻿namespace StudentService
{
    public class Student
    {
        public int StudentID { get; set; }

        public string StudentName { get; set; }

        public int Age { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Student student &&
                   StudentID == student.StudentID &&
                   StudentName == student.StudentName &&
                   Age == student.Age;
        }
    }
}
