﻿using System.Linq;

namespace StudentService
{
    public class StudentAggregationService
    {
        public Student[] Union(Student[] students, Student[] studentsToAggregate)
        {
            var aggregatedProducts = students.Union(studentsToAggregate);

            return aggregatedProducts.ToArray();
        }

    }

}
