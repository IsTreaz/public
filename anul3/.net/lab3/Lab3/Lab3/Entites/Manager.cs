﻿
namespace Lab3
{
    public class Manager : Employee
    {

        public Manager(string FirstName, string SecondName)
            :base(FirstName, SecondName)
        {

        }
        public override string Salutation()
        {
            return "Hello Dear Manager";
        }

    }
}
