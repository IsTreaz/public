﻿using System;

namespace Lab3
{
    public abstract class Employee
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public float Salary { get; set; }

        public Employee(string firstName, string secondName)
        {
            Id = new Guid();
            this.FirstName = firstName;
            this.SecondName = secondName;

            StartDate = DateTime.Now;
        }

        public string GetFullName()
        {
            return FirstName + " " + SecondName;
        }

        public bool IsActive()
        {
            if(EndDate > StartDate)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public abstract string Salutation();
    }
}
