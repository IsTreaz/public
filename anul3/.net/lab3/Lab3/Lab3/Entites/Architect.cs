﻿namespace Lab3
{
    public class Architect : Employee
    {

        public Architect(string FirstName, string SecondName)
            :base(FirstName, SecondName)
        {

        }
        public override string Salutation()
        {
            return "Hello Little Architect";
        }
    }
}
