﻿namespace Lab3.ExtendedMethods
{
    public static class StringExtension
    {
        public static int Splitter(this string txt)
        {
            return txt.Split().Length;
        }
    }
}
