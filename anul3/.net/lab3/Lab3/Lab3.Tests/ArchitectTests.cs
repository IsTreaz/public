using System;
using Xunit;

namespace Lab3.Tests
{
    public class ArchitectTests
    {
        Architect architect = new Architect(EmployeeFactory.GetFirstName(), EmployeeFactory.GetSecondName());

        [Fact]
        public void GivenGetFullName_WhenFirstNameAndSecondNameIsSet_ThenShouldReturnValidFullName()
        {
            // arrange
            var expected = EmployeeFactory.GetFullName();

            // act
            var result = architect.GetFullName();

            // assert
            Assert.Equal(result, expected);
        }

        [Fact]
        public void GivenIsActive_WhenEndDateIsNotSet_ThenShouldReturnTrue()
        {
            // arrange
            var expected = true;
            // act
            var result = architect.IsActive();

            // assert
            Assert.Equal(result, expected);
        }

        [Fact]
        public void GivenIsActive_WhenEndDateIsNotSet_ThenShouldReturnFalse()
        {
            // arrange
            var expected = false;
            architect.EndDate = DateTime.Now;
            // act
            var result = architect.IsActive();

            // assert
            Assert.Equal(result, expected);
        }

        [Fact]
        public void GivenSalutation_WhenArchitectIsCreated_ThenShouldReturnPropperString()
        {
            // arrange
            var expected = "Hello Little Architect";

            // act
            var result = architect.Salutation();

            // assert
            Assert.Equal(result, expected);
        }
    }
}
