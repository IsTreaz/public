﻿using System;
using Xunit;

namespace Lab3.Tests
{
    public class ManagerTests
    {
        Manager manager = new Manager(EmployeeFactory.GetFirstName(), EmployeeFactory.GetSecondName());

        [Fact]
        public void GivenGetFullName_WhenFirstNameAndSecondNameIsSet_ThenShouldReturnValidFullName()
        {
            // arrange
            var expected = EmployeeFactory.GetFullName();

            // act
            var result = manager.GetFullName();

            // assert
            Assert.Equal(result, expected);
        }

        [Fact]
        public void GivenIsActive_WhenEndDateIsNotSet_ThenShouldReturnTrue()
        {
            // arrange
            var expected = true;
            // act
            var result = manager.IsActive();

            // assert
            Assert.Equal(result, expected);
        }

        [Fact]
        public void GivenIsActive_WhenEndDateIsNotSet_ThenShouldReturnFalse()
        {
            // arrange
            var expected = false;
            manager.EndDate = DateTime.Now;
            // act
            var result = manager.IsActive();

            // assert
            Assert.Equal(result, expected);
        }

        [Fact]
        public void GivenSalutation_WhenManagerIsCreated_ThenShouldReturnPropperString()
        {
            // arrange
            var expected = "Hello Dear Manager";

            // act
            var result = manager.Salutation();

            // assert
            Assert.Equal(result, expected);
        }
    }
}
