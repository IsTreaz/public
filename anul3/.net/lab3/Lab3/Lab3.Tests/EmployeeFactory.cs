﻿namespace Lab3.Tests
{
    public static class EmployeeFactory
    {
        public static string GetFirstName()
        {
            return "Tibuleac";
        }
        public static string GetSecondName()
        {
            return "Eduard";
        }

        public static string GetFullName()
        {
            return "Tibuleac Eduard";
        }
    }
}
