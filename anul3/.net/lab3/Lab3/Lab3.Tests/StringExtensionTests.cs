﻿using Xunit;
using Lab3.ExtendedMethods;

namespace Lab3.Tests
{
    public class StringExtensionTests
    {
        [Fact]
        public void GivenSplitter_WhenATextWith3WordsIsSetAsParameter_ThenShouldReturn3()
        {
            // arrange 
            string text = "ala bala porto";
            var expected = 3;

            // act
            var result = text.Splitter();

            // act
            Assert.Equal(expected, result);
        }

        [Fact]
        public void GivenSplitter_WhenATextWithWordsUnseparated_ThenShouldReturn1()
        {
            // arrange 
            string text = "alaCbalaPporto";
            var expected = 1;

            // act
            var result = text.Splitter();

            // act
            Assert.Equal(expected, result);
        }
    }
}
