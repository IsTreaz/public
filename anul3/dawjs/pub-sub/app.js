const PubSub = (function() {
  /** ... */
  let events = {};
  events["counterClick"] = [];
  function subscribe(eventType, callback) {
    events[eventType].push(callback);
  }


  function dispatch(eventType, value) {
    events[eventType].forEach(callback => {
      callback(value);
    });
  }


  return { subscribe, dispatch };
})();

const btnWrapper1 = document.getElementById("buttonWrapper1");
const btnWrapper2 = document.getElementById("buttonWrapper2");
const counterWrapper1 = document.getElementById("counterWrapper1");
const counterWrapper2 = document.getElementById("counterWrapper2");

class Counter {
  /** ... */
  constructor(elem, value) {
    this.elemRef = elem;
    this.elemRef.innerHTML = value;

    PubSub.subscribe("counterClick", ((value) => {
      let baseValue = parseInt(this.elemRef.innerHTML);
      this.elemRef.innerHTML = baseValue + value;
      
    }).bind(this));
  }
}

class Button {
  constructor(elem, value) {
    this.elemRef = elem;
    this.elemRef.innerHTML = value;
    this.buttonValue = parseInt(value);
    
    this.elemRef.onclick = function() {
      PubSub.dispatch("counterClick", parseInt(elem.innerHTML));
    };
  }
}

new Counter(counterWrapper1, 10);
const x = new Counter(counterWrapper2, -100);
console.log(x);

// PubSub.dispatch("counterClick");
new Button(btnWrapper1, 7);
new Button(btnWrapper2, -3);

// Button and Counter don't interact directly. Only through the PubSub module!
