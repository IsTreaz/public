const parser = require('csv-parser');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const fs = require('fs');

const readStream = fs.createReadStream('./users.csv');
// const writeStream = fs.createWriteStream('./users.csv');

let users = [];

function parseData() {
  return new Promise((resolve) => {
    readStream.pipe(parser())
      .on('data', (row) => {
        users.push(row);
      }).on('end', () => {
        resolve();
      })
  })
}

function writeData() {
  const writer = createCsvWriter({
    path: 'users.csv',
    header: [
      {id: 'username', title: 'username'},
      {id: 'name', title: 'name'},
      {id: 'password', title: 'password'},
      {id: 'date', title: 'date'}
    ]
  });
  return writer.writeRecords(users);
}

function createUser({ username, name, password, date }) {
  return parseData().then(() => {
    users.push({ username, name, password, date });
    return writeData()
  });
}

function removeUser(username) {
  return parseData().then(() => {
    users = users.filter(user => user.username !== username);
    return writeData();
  });
}

function changePassword(username, password) {
  return parseData().then(() => {
    const user = users.filter(user => user.username === username);
    let newUser;
    if (user) {
      newUser = {...user, password};
    }
    users = users.filter(user => user.username !== username);
    users.push(newUser);
    return writeData();
  });
}

changePassword('ana123', 'thebest');

module.exports = {
  createUser,
  removeUser
}