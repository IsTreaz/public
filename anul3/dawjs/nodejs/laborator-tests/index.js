let http = require("http");
const fs = require("fs");

const PORT = 1234;

let server = http.createServer(handleRequest);

server.listen(PORT);
console.log(`listening on  ${PORT}`);

function handleRequest(req, res) {
  console.log(req.url);

  const path = "." + req.url;
  console.log(fs.existsSync(path));

  if (!fs.existsSync(path)) {
    res.writeHead(404, {
      "Content-type": "text/plain"
    });
    res.write("file not found");
    res.end();
  }
  // res.writeHead(200, {
  //   "Content-type": "text/plain"
  // });
  // res.write("Hello world");
  // res.end();
}
