const PORT = 1234;
const yamlFilePath = "./swagger.yaml";
const htmlFilePath = "./index.html";

let http = require("http");
const fs = require("fs");
const yaml = require("js-yaml");

let server = http.createServer(() => console.log("lawl"));

server.listen(PORT);
console.log(`listening on  ${PORT}`);

try {
  let doc = yaml.safeLoad(fs.readFileSync(yamlFilePath, "utf8"));
  fs.writeFile(htmlFilePath, "", error => {
    error ? console.error(error) : undefined;
  });
  //   doc = JSON.stringify(doc);

  Object.keys(doc).forEach(key => {
    tag = convertKeyValueIntoHtml(key, doc[key]);

    fs.appendFile(htmlFilePath, tag, error => {
      console.log(error ? error : undefined);
    });
  });

  //   fs.writeFile(htmlFilePath, doc, error => {
  //     console.log(error ? error : "file saved");
  //   });

  //   console.log(doc);
} catch (e) {
  console.error(e);
}

function isDict(obj) {
  return typeof obj === "object" && !Array.isArray(obj);
}
function isArray(obj) {
  return Array.isArray(obj);
}
function convertKeyValueIntoHtml(key, value) {
  //   console.log(typeof value === "object" && !Array.isArray(value));
  if (isDict(value)) {
    htmlTag = splitObject(value);
  } else {
    htmlTag = `<div style= "font-size:32px"> ${key} : <span style="color: blue">${value}</span> </div>`;
  }
  return htmlTag;
}
function splitObject(obj, level = 1) {
  result = "";
  Object.keys(obj).forEach(key => {
    // if (key === "x-origin") {
    //   console.log(obj[key]);
    //   console.log(isDict(obj[key]));
    // }
    if (isArray(obj[key])) {
      result += convertArrayIntoHtml(obj[key]);
      return result;
    }
    if (isDict(obj[key])) {
      result += splitObject(obj[key], level + 1);
    } else {
      htmlTag = `<div style = "margin-left: ${50 *
        level}; font-size:25px">${key} : <span style="color: lightblue">${
        obj[key]
      }</span> </div>`;
      result += htmlTag;
    }
  });

  return result;
}
function convertArrayIntoHtml(arr) {
  result = `<ul>`;
  arr.forEach(item => {
    result += "<li>";
    if (isDict(item)) result += splitObject(item, 0);
  });
  result += "</ul>";
  return result;
}
