let i18n = (function() {
    /* your code here */

    let language = {};

    function registerLanguage(languageKey, texts){
        language[languageKey] = {};
        Object.keys(texts).forEach( (key) => {
            language[languageKey][key] = texts[key]
        })
    }

    function applyLanguage(params) {
         const dictionary = language[params]
         
         let allTexts = document.querySelectorAll('[data-text]');
         allTexts.forEach( (elem) => {
            var dictionaryTextKey = elem.dataset.text;
            if(dictionary[dictionaryTextKey] !== undefined) {
                elem.innerHTML = dictionary[dictionaryTextKey]
            }
         })
         
    }

    function registerSentences(languageKey, texts) {
        Object.keys(texts).forEach( (key) => {
            language[languageKey][key] = texts[key]
        })
        return language;
    }
    function refresh() {
        applyLanguage(document.querySelector('body').dataset.lang);
    }

    return {registerLanguage, applyLanguage, registerSentences, refresh}
})();

const x =i18n.registerLanguage('en', {
    hello_there: 'Hey there',
    intro_paragraph: 'Welcome to this exercise'
});
try {
    i18n.applyLanguage(document.querySelector('body').dataset.lang);
} catch (err) {
    alert('unsupported language');
}

i18n.registerSentences('en', {
    later_text: 'Some later text'
});
i18n.refresh();