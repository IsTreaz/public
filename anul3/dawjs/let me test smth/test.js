// Implementati modului ActionManager impreuna
// cu clasele Plus2 si Minus 5, 
//pentru ca codul de mai jos sa functioneze conform exemplului.

let startNumber = 0;
let ActionManager = (function ActionManagerIIFE(number) {
    /** ... */
    last = number;
    let arr = [];
    arr.push(number);
    function doIt(object) {
        if( object.name === "Plus2") {
            last+=2;
            arr.push(last);
        }
        if( object.name === "Minus5") {
            last -= 5;
            arr.push(last)
        }
        return last;
    }

    function undo() {
        arr.pop();
        if(arr.length === 0) {
            throw 'Cannot redo anymore'
        }
        return arr[arr.length-1];
    }
    return {doIt, undo}
})(startNumber);

class Plus2 {
    
}

class Minus5 {
}

let x = ActionManager.doIt(Plus2); // returns 2

x = ActionManager.doIt(Minus5); // returns -3

x = ActionManager.undo(); // returns 2

x = ActionManager.undo(); // returns 0
console.log("TCL: x", x)

x = ActionManager.undo(); // throw an error: 'Cannot redo anymore'
