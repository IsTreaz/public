stopAfter(logName, 2000, 5);
// --> Should run the "logName" function every 2 seconds and stop after 5 times.
// --> In other words, you should see 5 times the string "Bob" at the console

function stopAfter(Cb, nr, max) {
  /** Your code here */
  const interval = setInterval(
    () => {
      Cb();
      if (--max === 0) clearInterval(interval);
    },
    nr,
    max
  );
}

function logName() {
  console.log("Bob");
}
