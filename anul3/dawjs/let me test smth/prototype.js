Array.prototype.getIndexOf = function(object) {
  this.index = -1;
  this.map((value, index) => {
    if (value === object) {
      this.index = index;
    }
  });
  return this.index;
};

Array.prototype.iterate = function ArrayIterate(callback) {
  for(let i=0;i<this.length;i++) {
    callback(this[i])
  }
};

Array.prototype.transform = function ArrayTransform (callback) {
    /** ... */
    let arr = this
    for(let i=0;i<this.length;i++) {
        arr[i] = callback(arr[i], i, this)
    }
    return arr;
}

Array.prototype.keep = function ArrayKeep (checkCallback) {
    /** ... */

    let arr = [];
    
    this.forEach(x => {
        
        if(checkCallback(x) === true) {
            arr.push(x)
        }
    })
    
    this.forEach(x => this.pop());
    this.pop();

    arr.forEach( x => this.push(x));
}

Array.prototype.smoosh = function ArraySmoosh () {
    /** ... */
    let newArray = [];

    this.forEach( x => {
        if(Array.isArray(x)) {
            x.forEach( elem => newArray.push(elem))
        } else {
            newArray.push(x);
        }
    })
    return newArray;
}




class Plus {
  constructor(x,y) {
    if(x instanceof Minus) {
      this.result = x.result;
    }
    else {
      this.result = x;
    }
    
    if(y instanceof Minus) {
      this.result += y.result;
    } else {
      this.result += y;
    }
  }
}

class Minus {
  constructor (x,y) {
    if(x instanceof Plus) {
      this.result = x.result;
    } else {
      this.result = x;
    }

    if(y instanceof Plus) {
      this.result -= y.result
    } else {
      this.result -= y
    }
  }
}

class log {
    static print(x){
      if(x instanceof Plus || x instanceof Minus){
        console.log(x.result);
      }
    }
}

log.print(new Plus(new Minus(5,10), 15));
log.print(new Plus(15, new Minus(5,10)));


// console.log(arr);
// console.log(arr);
// a

// console.log(Array);
