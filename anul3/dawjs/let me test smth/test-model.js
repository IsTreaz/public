// Implementati modului CopyPasteManager
//  impreuna cu clasele TextData, Paper si Reader,
//  pentru ca codul de mai jos sa functioneze conform exemplului.

let CopyPasteManager = (function CopyPasteManagerIIFE() {
  /** ... */
  // this.texts = []
  let txt = "";
  function copy(textData) {
    txt = textData.getText();
  }

  function paste(paper) {
    paper.setText(txt);
  }

  function cut(textData) {
    txt = txt + textData.getText();
    textData.setText("");
  }

  return { copy, paste, cut };
})();

class TextData {
  /** ... */
  constructor(string) {
    this.text = string;
  }

  getText() {
    return this.text;
  }
  setText(string) {
    this.text = string;
  }
}

class Paper {
  constructor() {
    this.text = "";
  }

  setText(text) {
    this.text = text;
  }
}

class Reader {
  constructor() {}

  read(paper) {
    if (paper.text === "") {
      throw " text is not there";
    } else {
      console.log(paper.text);
    }
  }
}

let txt = new TextData("Hello!");

let paper1 = new Paper();
let paper2 = new Paper();

let reader = new Reader();

CopyPasteManager.copy(txt);
CopyPasteManager.paste(paper1);
CopyPasteManager.paste(paper2);

reader.read(paper1); // Reading: 'Hello!'
reader.read(paper2); // Reading: 'Hello!'

CopyPasteManager.cut(txt);
CopyPasteManager.paste(paper1);

reader.read(paper1); // Reading: 'Hello!Hello!';
reader.read(txt); // throw an error: 'The text is not there'
