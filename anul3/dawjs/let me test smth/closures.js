const add = (x, y) => x + y;
const sub = (x, y) => x - y;
const inmult = (x, y) => x * y;

let tenF = identifyF("hi");

function identifyF(x) {
  return function() {
    return x;
  };
}

function addF(x) {
  return function(y) {
    return add(x, y);
  };
}

function genericF(fn) {
  return function firstCall(x) {
    return function secondCall(y) {
      return fn(x, y);
    };
  };
}

function reverseF(fn) {
  return function(x, y) {
    return fn(y, x);
  };
}

function curry(sub, x) {
  return function(y) {
    return sub(x, y);
  };
}

function addG(x) {
  if (x === undefined) {
    return 0;
  }
  return function(y) {
    if (y === undefined) {
      return x;
    }
    return addG(x + y);
  };
}

function genericG(fn) {
  function call(x) {
    if (x === undefined) {
      return 0;
    }

    return function(y) {
      if (y === undefined) {
        return x;
      }
      return call(fn(x, y));
    };
  }

  return call;
}

function limit(fn, number) {
  return function(x, y) {
    number--;
    if (number < 0) {
      return undefined;
    }
    return fn(x, y);
  };
}

function counter(count) {
  function up() {
    count++;
    return count;
  }
  function down() {
    count--;
    return count;
  }

  return {up, down};
}
const c = counter(5);
console.log(c.up())
console.log(c.down())
var a=5;

