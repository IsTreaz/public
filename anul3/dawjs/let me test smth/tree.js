class Tree {
    /** ... */
}

class TreeNode {
    /* ... */
}

const myTree = new Tree();
const rootNode = new TreeNode(22);

rootNode.putLeft(new Node(11));
rootNode.putRight(new Node(12));

rootNode.getLeft().putLeft(new Node(10));
rootNode.getRight().putRight(13);


myTree.setRoot(rootNode);

myTree.print();  // 22 11 10 12 13