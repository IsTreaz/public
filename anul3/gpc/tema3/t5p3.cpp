#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <float.h>
#include <cmath>

#include "GL\glut.h"
#define dim 300

unsigned char prevKey;

class GrilaCarteziana {
public:
	GrilaCarteziana()
	{
		this->dim_x = 15.0;
		this->dim_y = 15.0;
		this->weight = 300.0;
		this->height = 300.0;
		square_line = (this->weight * 2) / (dim_x + 2);
	}

	GrilaCarteziana(double dim_x,double dim_y, double weight, double height)
	{
		this->dim_x = dim_x;
		this->dim_y = dim_y;
		this->height = height;
		this->weight = weight;
		square_line = ((weight > height ? height : weight) * 2) / ((dim_x>dim_y?dim_x:dim_y) + 2);
	}

	~GrilaCarteziana() {}

	double solve_x(unsigned int x) {
		double real_x = (-1) + square_line / weight;
		double pas_x = square_line / weight;
		for (unsigned int i = 0; i < x; i++)
			real_x += pas_x;
		return real_x;
	}
	double solve_y(unsigned int y) {
		double real_y = -1 + square_line / height;
		double pas_y = square_line / height;
		for (unsigned int i = 0; i < y; i++)
			real_y += pas_y;
		return real_y;

	}
	void writePixel(unsigned int x, unsigned int y) {
		glColor3f(0.0, 0.0, 0.0);
		glBegin(GL_POINTS);
		double ratie_x = 0.001*weight;
		double ratie_y = 0.001*height;
		double raza = 0.3*square_line;
		double real_x = solve_x(x);
		double real_y = solve_y(y);

		for (double x_iter = (real_x*weight - raza); x_iter <= (real_x*weight + raza); x_iter += ratie_x)
			for (double y_iter = (real_y*height - raza); y_iter <= (real_y*height + raza); y_iter += ratie_y) {
				//printf("%f %f\n", x_iter*x_iter, y_iter*y_iter);
				if (((x_iter - real_x * weight)*(x_iter - real_x * weight) + (y_iter - real_y * height)*(y_iter - real_y * height)) < (raza * raza))
					glVertex2d(x_iter / weight, y_iter / height);
			}
		glEnd();
	}

	void drawLine(int x0, int y0, int xn, int yn,unsigned int grosime=0) {
		int dx = xn - x0;
		int dy = yn - y0;

		if (abs(dx) > abs(dy)) { // abs(m)<=1

			if (xn < x0) {
				int aux = x0;
				x0 = xn;
				xn = aux;
				aux = y0;
				y0 = yn;
				yn = aux;
				dx = -dx;
			}
			int y_ratio = 1;
			if (y0 > yn)
			{
				y_ratio = -1;
				dy = -dy;
			}
			int x = x0;
			int y = y0;
			//writePixel(x, y);
			for (int gros = (-1)*((int)grosime); gros <= ((int)grosime); gros += 1) {
				if (((int)y - gros) >= 0 && ((int)y - gros) <= dim_y)
					writePixel(x, y - gros);
			}
			int d = 2 * dy - dx;
			int dE = 2 * dy;
			int dNE = 2 * (dy - dx);
			while (x < xn) {
				if (d <= 0) {//aleg E
					d += dE;
				}
				else {
					//aleg NE
					d += dNE;
					y += y_ratio;
				}
				x += 1;
				for (int gros = (-1)*((int) grosime); gros <= ((int)grosime); gros += 1) {
					if(((int)y - gros ) >= 0 && ((int)y - gros ) <= dim_y)
					writePixel(x, y- gros);
				}
				//writePixel(x, y);
			}

		}
		else
		{
			// abs(m)>1
			if (yn < y0) {
				int aux = x0;
				x0 = xn;
				xn = aux;
				aux = y0;
				y0 = yn;
				yn = aux;
				dy = -dy;
			}
			int x_ratio = 1;
			if (x0 > xn)
			{
				x_ratio = -1;
				dx = -dx;
			}
			int x = x0;
			int y = y0;
			//writePixel(x, y);
			for (int gros = (-1)*((int)grosime); gros <= ((int)grosime); gros += 1) {
				if (((int)x - gros) >= 0 && ((int)x - gros) <= dim_x)
					writePixel(x-gros, y);
			}
			int d = 2 * dx - dy;
			int dE = 2 * dx;
			int dNE = 2 * (dx - dy);

			while (y < yn) {
				if (d <= 0) {//aleg E
					d += dE;
				}
				else {
					//aleg NE
					d += dNE;
					x += x_ratio;
				}
				y += 1;
				//writePixel(x, y);
				for (int gros = (-1)*((int)grosime); gros <= ((int)grosime); gros += 1) {
					if (((int)x - gros) >= 0 && ((int)x - gros) <= dim_x)
						writePixel(x - gros, y);
				}
			}

		}

	}
	void display()
	{

		// desenez grila
		glColor3f(0.0, 0.0, 0.0);
		glLineWidth(2);
		glBegin(GL_LINES);
		unsigned int minim = (height > weight ? weight : height);
		double maximum_x = (-1) + (square_line * (dim_x + 1)) / weight;
		double minimum_x = (-1) + square_line / weight;
		double minimum_y = (-1) + square_line / height;
		double maximum_y = (-1) + (square_line * (dim_y + 1)) / height;
		double pas_x = square_line / weight;
		double pas_y = square_line / height;
		double x;
		unsigned int counter = 0;
		for (x = minimum_x; x <= maximum_x; x += pas_x) {
			counter += 1;
			glVertex2f(x, maximum_y);
			glVertex2f(x, minimum_y);
		}
		if (counter < (dim_x + 1)) {
			glVertex2f(x, maximum_y);
			glVertex2f(x, minimum_y);
		}
		double y;
		counter = 0;
		for (y = minimum_y; y <= maximum_y; y += pas_y) {
			counter += 1;
			glVertex2f(minimum_x, y);
			glVertex2f(maximum_x, y);
		}
		if (counter < (dim_y + 1)) {
			glVertex2f(minimum_x, y);
			glVertex2f(maximum_x, y);
		}
		glEnd();

		// desenez cele 2 linii
		glColor3f(0.9, 0.0, 0.0);
		glBegin(GL_LINES); glLineWidth(2);
		glVertex2d(solve_x(0), solve_y(0));
		glVertex2d(solve_x(15), solve_y(7));
		glVertex2d(solve_x(0), solve_y(15));
		glVertex2d(solve_x(15), solve_y(10));
		glEnd();

		drawLine(0, 0, 15, 7);
		drawLine(0, 15, 15, 10,1);
		//drawLine(0, 0, 3, 15, 1);

	}
	double weight, height, dim_x,dim_y;

	double square_line;
};

//GrilaCarteziana grila(15.0,17.0,300,300);
GrilaCarteziana grila;
void Init(void) {

	glClearColor(1.0, 1.0, 1.0, 1.0);

	glLineWidth(1);

	//   glPointSize(3);

	glPolygonMode(GL_FRONT, GL_LINE);
}

void Display(void) {
	glClear(GL_COLOR_BUFFER_BIT);
	grila.display();
	glFlush();
}

void Reshape(int w, int h) {
	unsigned int dim_x_copy = grila.dim_x; unsigned int dim_y_copy = grila.dim_y;
	GrilaCarteziana grl2(dim_x_copy,dim_y_copy, w, h);
	grila = grl2;
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}

void KeyboardFunc(unsigned char key, int x, int y) {
	prevKey = key;
	if (key == 27) // escape
		exit(0);
	glutPostRedisplay();
}

void MouseFunc(int button, int state, int x, int y) {
}

int main(int argc, char** argv) {

	glutInit(&argc, argv);

	glutInitWindowSize(dim, dim);

	glutInitWindowPosition(100, 100);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutCreateWindow(argv[0]);

	Init();

	glutReshapeFunc(Reshape);

	glutKeyboardFunc(KeyboardFunc);

	glutMouseFunc(MouseFunc);

	glutDisplayFunc(Display);

	glutMainLoop();

	return 0;
}


