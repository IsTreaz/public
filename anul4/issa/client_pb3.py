import socket
import sys
import json
import ast
import time

file = open('test.json',)
data_json = json.load(file)

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('localhost', 3272)
print(sys.stderr, 'connecting to %s port %s' % server_address)
sock.connect(server_address)

status = 'closed'
data_received = 0
longitudeA = 0
latitudeA = 0
longitudeB = 0
latitudeB = 0
print(data_json)
print("long:  ", data_json["longitude"])
print("lat: ", data_json["latitude"])

try:
        i=0
        while(i<2):
            data = sock.recv(100).decode()
            if(data[0] == '0'):
                sock.sendall(('lat   ' + str(data_json['latitude'])).encode())
            if(data[0] == '1'):
                sock.sendall(('long  ' + str(data_json['longitude'])).encode())

        input()

finally:
    print(sys.stderr, 'closing socket')
    sock.close()
