import sys
import json
import ast
import time
import socket

file = open('test.json',)
data_json = json.load(file)

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('localhost', 3602)
print(sys.stderr, 'connecting to %s port %s' % server_address)
sock.connect(server_address)

status = 'closed'
data_received = 0
longitudeA = 0
latitudeA = 0
longitudeB = 0
latitudeB = 0

print("Initial longitude is: ", data_json["longitude"])
print("Initial latitude is: ", data_json["latitude"])

try:
    while status == 'closed' or status == 'open':
        data = sock.recv(100).decode()
        if data == 'open':
            status = 'open'
            print("Geofence is opened")
        elif data == 'closed':
            status = 'closed'
            print("Geofence is closed")
        if status == 'open':
            if ':' in data:
                print(data)
                response = ast.literal_eval(data)
                if response["longitude"] == '' or response["latitude"] == '':
                    break
                if data_received == 0:
                    longitudeA = response["longitude"]
                    latitudeA = response["latitude"]
                    data_received += 1
                elif data_received == 1:
                    longitudeB = response["longitude"]
                    latitudeB = response["latitude"]
                    if data_json["longitude"] > float(longitudeA) and data_json["longitude"] < float(longitudeB):
                        if data_json["latitude"] > float(latitudeA) and data_json["latitude"] < float(latitudeB):
                            print("Coordinates are in the rectangle set")
                            sock.sendall("Coordinates are valid".encode())
                        else:
                            print("Coordinates are invalid (latitude)")
                            sock.sendall(
                                "Coordinates are invalid (latitude)".encode())
                    else:
                        print("Coordinates are invalid (longitude)")
                        sock.sendall(
                            "Coordinates are invalid (longitutde)".encode())
                    data_received = 0
        elif status == 'closed':
            sock.sendall("Geofence is closed".encode())
            input()

finally:
    print(sys.stderr, 'closing socket')
    sock.close()