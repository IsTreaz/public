import sys
import json
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('localhost', 3603)
print(sys.stderr, 'starting up on %s port %s' % server_address)
sock.bind(server_address)

sock.listen(1)

while True:
    print(sys.stderr, 'connection loading...')
    connection, client_address = sock.accept()
    try:
        print(sys.stderr, 'connection from', client_address)
        while True:
            print("Status of geofence: ")
            geofence_status = input()
            connection.sendall(geofence_status.encode())
            setA = {}
            setB = {}
            print("Input A")
            longitudeA = input()
            latitudeA = input()
            setA["longitude"] = longitudeA
            setA["latitude"] = latitudeA
            dataA_serialized = json.dumps(setA)
            connection.sendall(dataA_serialized.encode())
            print("Input B")
            longitudeB = input()
            latitudeB = input()
            setB["longitude"] = longitudeB
            setB["latitude"] = latitudeB
            dataB_serialized = json.dumps(setB)
            connection.sendall(dataB_serialized.encode())
            data = connection.recv(100).decode()
            print('received "%s"' % data)
    finally:
        connection.close()