import gi
gi.require_version('Gtk', '3.0')

import dbus

from gi.repository import Gtk
gi.require_version('GdkX11', '3.0')

from gi.repository import GdkX11

import bluetooth
from xml.etree import ElementTree
from PyOBEX import client
from PyOBEX import headers
from PyOBEX import responses
import vobject
import threading
from queue import Queue


devices = bluetooth.discover_devices(duration=1, lookup_names=True)

gtkBox = 1
class PopUp(Gtk.Dialog):

    def __init__(self, parent):
        Gtk.Dialog.__init__(self, "Contacts downloading", parent, Gtk.DialogFlags.MODAL)
        self.set_default_size(50, 50)
        self.set_border_width(50)

        area = self.get_content_area()
        area.add(Gtk.Label("Contactele se descarca... Apasa OK pentru continuare"))
        self.show_all()



class ApplicationWindow(Gtk.Window):

    resultModel = Gtk.ListStore(str)
    resultCombo = Gtk.ComboBox.new_with_model_and_entry(resultModel)

    label = Gtk.Label(" ")
    
    def __init__(self):
        Gtk.Window.__init__(self, title="Media Player")
        self.connect("destroy",Gtk.main_quit)
        

    def clear_contacts_combo_box(self):
        self.resultCombo.clear()
            
    def show(self):
        self.show_all()

    def bluetooth_devices(self, widget, data=None):
        print("Searching for devices...")
        devices = bluetooth.discover_devices(lookup_names=True)
        for addr, name in devices:
            print(name, addr)
            #self.resultModel.append([name,addr])
        

    def setup_objects_and_events(self):

        self.bluetooth_button = Gtk.Button()
        self.bluetooth_image = Gtk.Image.new_from_icon_name( "preferences-system-bluetooth", Gtk.IconSize.DND)
        self.bluetooth_button.set_image(self.bluetooth_image)
        self.bluetooth_button.connect("clicked", self.bluetooth_devices)
        
        self.playback_button = Gtk.Button()
        self.play_image = Gtk.Image.new_from_icon_name( "gtk-media-play", Gtk.IconSize.DND)
        self.playback_button.set_image(self.play_image)
        self.playback_button.connect("clicked", self.play_music)

        self.pause_button = Gtk.Button()
        self.pause_image = Gtk.Image.new_from_icon_name( "gtk-media-pause", Gtk.IconSize.DND)
        self.pause_button.set_image(self.pause_image)
        self.pause_button.connect("clicked", self.pause_music)

        self.stop_button = Gtk.Button()
        self.stop_image = Gtk.Image.new_from_icon_name( "gtk-media-stop", Gtk.IconSize.DND)
        self.stop_button.set_image(self.stop_image)
        self.stop_button.connect("clicked", self.stop_music)

        self.next_button = Gtk.Button()
        self.next_image = Gtk.Image.new_from_icon_name( "gtk-media-next", Gtk.IconSize.DND)
        self.next_button.set_image(self.next_image)
        self.next_button.connect("clicked", self.next_music)

        self.previous_button = Gtk.Button()
        self.previous_image = Gtk.Image.new_from_icon_name( "gtk-media-previous", Gtk.IconSize.DND)
        self.previous_button.set_image(self.previous_image)
        self.previous_button.connect("clicked", self.previous_music)

        self.rewind_button = Gtk.Button()
        self.rewind_image = Gtk.Image.new_from_icon_name( "gtk-media-rewind", Gtk.IconSize.DND)
        self.rewind_button.set_image(self.rewind_image)
        self.rewind_button.connect("clicked", self.rewind_music)

        self.volume_up_button = Gtk.Button()
        self.volume_up_image = Gtk.Image.new_from_icon_name( "audio-volume-high", Gtk.IconSize.DND)
        self.volume_up_button.set_image(self.volume_up_image)
        self.volume_up_button.connect("clicked", self.volume_up_music)

        self.volume_down_button = Gtk.Button()
        self.volume_down_image = Gtk.Image.new_from_icon_name( "audio-volume-low", Gtk.IconSize.DND)
        self.volume_down_button.set_image(self.volume_down_image)
        self.volume_down_button.connect("clicked", self.volume_down_music)

        self.combo = Gtk.ComboBoxText()
        for i in range(0, len(devices)):
            addr, name = devices[i]
            self.combo.append(str(i), name)
        self.combo.connect("changed", self.on_comboboxtext_changed)

        self.grid = Gtk.Grid()
        self.add(self.grid)
        self.grid.attach(self.playback_button, 5, 0, 1, 1)
        self.grid.attach(self.pause_button, 4, 0, 1, 1)
        self.grid.attach(self.stop_button, 6, 0, 1, 1)
        self.grid.attach(self.next_button, 7, 0, 1, 1)
        self.grid.attach(self.previous_button, 3, 0, 1, 1)
        self.grid.attach(self.rewind_button, 5, 1, 1, 1)
        self.grid.attach(self.volume_up_button, 6, 1, 1, 1)
        self.grid.attach(self.volume_down_button, 4, 1, 1, 1)
        self.grid.attach(self.resultCombo, 4, 8, 3, 1)
        self.grid.attach(self.combo,4,5,1,1)
        
        self.downloadContacts_button = Gtk.Button()
        self.downloadContacts_image = Gtk.Image.new_from_icon_name("emblem-downloads", Gtk.IconSize.DND)
        self.downloadContacts_button.set_image(self.downloadContacts_image)
        self.downloadContacts_button.connect("clicked", self.download_contacts)
        
        self.grid.attach(self.downloadContacts_button, 7, 8, 1, 1)

        self.grid.attach(self.label, 3, 4, 3, 1)
           
    def play_music(self, widget, data=None):
        hal_manager_media_interface.Play()
    def pause_music(self, widget, data=None):
        hal_manager_media_interface.Pause()
    def stop_music(self, widget, data=None):
        hal_manager_media_interface.Stop()
    def next_music(self, widget, data=None):
        hal_manager_media_interface.Next()
    def previous_music(self, widget, data=None):
        hal_manager_media_interface.Previous()
    def rewind_music(self, widget, data=None):
        hal_manager_media_interface.Rewind()
    def volume_up_music(self, widget, data=None):
        hal_manager_media_interface.VolumeUp()
    def volume_down_music(self, widget, data=None):
        hal_manager_media_interface.VolumeDown()

    def on_comboboxtext_changed(self, comboboxtext):
        addr = devices[comboboxtext.get_active()][0]
        self.address = addr
        print(addr)
    def download_contacts(self, widget, data=None):
        self.clear_contacts_combo_box()

        dialog = PopUp(self)
        self.label.set_text("Terminat")

        device_address = self.address
        prefix = ""
        d = bluetooth.find_service(address=device_address, uuid="1130")
        if not d:
            print("No Phonebook service found.\n")

        port = d[0]["port"]

        c = client.Client(device_address, port)
        uuid = b"\x79\x61\x35\xf0\xf0\xc5\x11\xd8\x09\x66\x08\x00\x20\x0c\x9a\x66"
        result = c.connect(header_list=[headers.Target(uuid)])

        if not isinstance(result, responses.ConnectSuccess):
            print("Failed to connect to phone.\n")

        hdrs, cards = c.get(prefix+"telecom/pb", header_list=[headers.Type(b"x-bt/vcard-listing")])

        root = ElementTree.fromstring(cards)

        print("\nAvailable cards in %stelecom/pb\n" % prefix)
        names = []

        for card in root.findall("card"):
            try:
                names.append(card.attrib["handle"])
            except UnicodeEncodeError:
                pass

        # print("\nCards in %stelecom/pb\n" % prefix)

        c.setpath(prefix + "telecom/pb")
        i=0
        for name in names:
            i=i+1
            hdrs, card = c.get(name, header_list=[headers.Type(b"x-bt/vcard")])
            encoding = 'utf-8'
            strCard = card.decode(encoding)
            vCard = vobject.readOne(strCard)
            vCard.prettyPrint()
            if hasattr(vCard,'n'):
                if hasattr(vCard,'tel'):
                    ApplicationWindow.resultModel.append([vCard.n.value.family + ' ' + vCard.n.value.given + ' ' + vCard.tel.value])
 
        cell = Gtk.CellRendererText()
        ApplicationWindow.resultCombo.pack_start(cell,False)
        ApplicationWindow.resultCombo.add_attribute(cell,"text",0)
        c.disconnect()
        dialog.destroy()



if __name__ == '__main__':
    window = ApplicationWindow()
    bus = dbus.SystemBus()
    hal_manager_object = bus.get_object('org.bluez',
                                       '/org/bluez/hci0/dev_90_8C_43_F2_83_D0')
    hal_manager_media_interface = dbus.Interface(hal_manager_object, 'org.bluez.MediaControl1')

    window.setup_objects_and_events()
    
    window.show()

    Gtk.main()


